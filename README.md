## Usage

Please use NintendulatorDX with these files. Find it here:
https://fo.aspekt.fi/downloads/nintendulatordx-v36.zip

To try the example, please place `ndxdebug.h` in the `example/header` folder.
It can be found in the above package or here:
https://github.com/fo-fo/nintendulatordx/blob/master/features-demo/src/ndxdebug.h

If ca65 is in the path, execute `build.cmd` in the `example` folder to try the demo.

## Summary:

These macros allow declaration, definition and calling of functions with parameters.
Functions use a user defined space in zeropage for both parameters and local variables.

### Declarations:

Declaring functions is not required. Forward declarations allow functions to be imported, or calls to be made before the
full definition. Functions can be declared by invoking the <declarefunc> macro with the first parameter being the 
new function name, the second parameter are the function parameters enclosed in curly brackets.
The macro will use *.global* to allow for the import the function or definition later in the same source file.

### Definition:

Functions can be defined by invoking the <func> macro with the first parameter being the function name,
and the second, the parameters enclosed in curly brackets. The parameters must match the parameters used before if
the function was first named with <declarefunc>. It will check for matching names, and memory sizes of parameters.

### Calling:

The call will verify the parameters and attempt to optimize the order of parameter loading.
Functions can be called by invoking the <call> macro with the first parameter being the function name,
and the parameters following enclosed in curly braces. Parameters in the call can be in any order when
using the identifiers declared in the definition. Parameters can be omitted (they will be undefined).
**Calls can also be made without using the names of the parameters. In this case, they must be in the same
order as the declaration.**

#### Parameters:

Parameters can be defined as a valid ca65 identifier, followed by a size or register designation.
Valid parameter sizes/types:

    .byte, .word, .addr, .dword, a, x, y, ax, ay, xy


#### Local variables:

Locals can be defined inside a function body. They use the same memory area as the parameter memory.
They can be followed by a constant to define multiple.

Valid locals sizes:

    .byte, .word, .addr, .dword


### Declaring a function:

This example will create a parameter list with two byte size parameters.
The assembler will save all information needed as identifiers.

Examples:

Define a function:

    func add, { one .byte, two .byte }

    locals
        sum .word
    endlocals

        lda param one
        clc
        adc param two
        ldx #$00
        bcc :+
        inx
        :
        rts

    endfunc

### Calling a function:


    call addTwo, { one: #$10, two: foo }

Or, the same result without named parameters:

    call addTwo, { #$10, foo }

The macro will load all parameters as requested before using a JSR to call the function.
Note: Registers names can be referenced directly as well, the defined parameter names for registers are not required.
Registers can be used for source values or parameters.

    call myFunc, { foo: #$10, x: <BAZ, a : a }

In the last example, no code will be output regarding register a, but it should still be included to indicate
that it is a needed parameter to avoid register a being used to load the other parameters.

#### Operators:

When calling, you can use normal ca65 syntax including the following operators:

Immediate value: `#`

Lowbyte: `<`

Highbyte: `>`

also:

Address: `&`

The address operator will force the value to be interpreted as an immediate word size value, implying an address is being passed.

Note: If there is a need, values can be prefixed with one of these operators to force a size for the value.
If there is a problem with the size of the value, a size can also be forced with a cast.

Example:

    call myFunc, { foo: (.byte)baz, bar: (.word)fum + 3 }  

#### Locals:

Locals can be defined inside a function body. They use the same memory as the parameter memory.

Example:

    func copyMem, { source ax, dest .addr, bytes y }

        locals 
            source  .addr
        endlocals

        sta local source
        stx local source + 1
        :
            lda (local source),y
            sta (param dest), y
        dey
        bne :-

        rts
    endfunc

Note: use **param** before a parameter name to access parameters.
Use **local** before a local name.

Use **fparam** to access function parameters after a call:

    lda fparam MyFunc::sum 
    lda {fparam MyFunc::sum}+1

### Nested Calls:

If a function that has parameters and/or locals is going to call a function, use the following syntax:
For a function that is going to be called from within a function:

    declarefunc myfunc, { f .byte }, 7

Any functions that will call **myfunc** can use up to 7 bytes of memory without conflict.
This is not needed if it is clear there are no conflicts.

Known limitations:

- A call to a function that loads registers explicitly will not check if the declaration declared the use of registers,
  meaning you can always explicitly load registers for the call.
- When calling a function using parameter names, parameters can be loaded more than once. The call will work, just wasteful.
- Trying to load x with y or y with x in the parameters will fail due to the limitation of the 6502: These macros don't use any additional temporary memory.
- Loading registers out of order may fail with en error. Register sources are loaded first. Example failed parameter list: { a: x, y: a }
- The high byte(s) of parameters loaded with a smaller source will be cleared to zero, except when using a CPU register as a source - only one byte will be stored.
- Sign extending not supported.
- The calling macro tries to verify the passed parameter isn't bigger than the declared parameter. It is sometimes difficult with ca65 to 
   determine the size of an identifier without providing the full scope, so the macro will default to the destination size
   for parameters when the identifier size cannot be determined.

Please use NintendulatorDX with these files. Find it here:

https://fo.aspekt.fi/downloads/nintendulatordx-v36.zip