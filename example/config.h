; --------------------------------------------------------------------------------------------
; Project configuration file
; This goes at the start of every separate module
; --------------------------------------------------------------------------------------------

; CONSTANTS for build environment:
.ifndef _CONFIG_H_
_CONFIG_H_ = 1

.setcpu "6502X" ; 'illegal' instructions
.linecont +     ; line continue feature

; --------------------------------------------------------------------------------------------
; General debugging:
; --------------------------------------------------------------------------------------------
DEBUG = 1	; for ndxdebug.h
__DEBUG__ = 1
__VERBOSE_MESSAGES__ = 1


; --------------------------------------------------------------------------------------------
; For funcCall.h:
; --------------------------------------------------------------------------------------------
 _PARAMETER_MEMORY_START_ = 0
; how much space parameters can use
_PARAMETER_MEMORY_MAXSIZE_ = 16
; _CALL_WARNINGS_ 1 = warn and display all nested calls. 0 = turn off
_CALL_WARNINGS_ = 0
; --------------------------------------------------------------------------------------------

; --------------------------------------------------------------------------------------------
; For ppu.inc:
; --------------------------------------------------------------------------------------------
__VRAM_BUFFER_SIZE__    = 255
__ATTRIB_SHADOW_SIZE__  = 64    ; one screen = 64, two screens = 128

.endif
