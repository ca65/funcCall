; --------------------------------------------------------------------------------------------
;
; reset.inc
;
; NOTE *
; A function, rootState must be defined

.ifndef _RESET_
_RESET_ = 1

.include "config.h"
.include "header/nmi.h"
.include "header/ppu.h"
.include "header/funcCall.h"
.include "header/ndxdebug.h"

.segment "CODE"

.scope reset_

.export resetRoutine

; --------------------------------------------------------------------------------------------

resetRoutine:
    
    sei             ; ignore IRQs
    cld             ; disable decimal mode
    ldx #$00
    stx PPU_CTRL    ; Disable NMI
    stx PPU_MASK    ; Disable rendering
    stx $4010       ; Disable DMC IRQ
    dex             ; LDX #$FF
    txs             ; Set stack
    bit SND_MASTERCTRL_REG      ; Acknowledge possible DMC IRQ
    lda #$40
    sta SND_APU_FRAME           ; Disable APU Frame IRQ
    lda #$0F
    sta SND_MASTERCTRL_REG      ; Disable DMC playback
    
    ; PPU warm up:
    bit PPU_STATUS
    
  : bit PPU_STATUS
    bpl :-
           
    inx ; #$00
    txa
    ldy #$FF
    :
        sta $00,x
        sta $0100,x
        tya
        sta $0200,x
        lda #$00
        sta $0300,x
        sta $0400,x
        sta $0500,x
        sta $0600,x
        sta $0700,x
    inx
    bne :-
    
  : bit PPU_STATUS
    bpl :-
    
    ndxLuaExecStr "print ('POWER ON/ RESET:')"
    ; execute lua code that needs init:
    ; include all lua libraries here:
    
    ndxLuaExecFile "library/funcCall.lua"
    funcCallTrackingVerbose 0
  
    call ppu::clearNameTable, { #>NAME_TABLE_0_ADDRESS , #0, #0 }
    ; not needed if only using one nametable:
    ; call ppu::clearNameTable, { nTable: #>NAME_TABLE_1_ADDRESS , tile: #0, attrib: #0 }
    ; not needed unless we have more nametable ram:
    ; call ppu::clearNameTable, { nTable: #>NAME_TABLE_2_ADDRESS , tile: #0, attrib: #0 }
    ; call ppu::clearNameTable, { nTable: #>NAME_TABLE_3_ADDRESS , tile: #0, attrib: #0 }
    
    ; --------------------------------------------------------------------------------------------
    ; system initialization is done:
    ; --------------------------------------------------------------------------------------------
    
    ; Turn on NMI before MainLoop, never turn it off (video still off) 
    
    ; set empty buffer
    lda #$FF
    sta ppu::vramBuffer
    
    lda #0
    staPPUmaskBits
    staPPUctrlBits #CT_NMI, 1 ; turn NMI on now
    
    call nmi::waitForNMI
    .import ___entryFromReset
    jmp ___entryFromReset
 
; end reset

; --------------------------------------------------------------------------------------------
 
.endscope
.endif
