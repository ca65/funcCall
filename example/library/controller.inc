; --------------------------------------------------------------------------------------------
; File: controller.inc


.ifndef _Controller_
_Controller_ = 1

.include "header/funcCall.h"
.include "header/ndxdebug.h"

.scope _Controller_ 

.pushseg
.segment "ZEROPAGE"

    pressed:          .res    2    ; new this frame - held down
    pressedLastFrame: .res    2    ; last frame / call
    pressedNew:       .res    2    ; newly pressed since last frame
    releasedNew:      .res    2    ; newly released since last frame
    
.popseg

declarefunc readPort, { player x } 

.export pressed             
.export pressedLastFrame       
.export pressedNew          
.export releasedNew         

; --------------------------------------------------------------------------------------------
; 
; controller to read in x

func readPort, { player x }

    lda pressed,x         ; save last frame's joystick
    sta pressedLastFrame,x

    lda #1
    sta pressed,x         ; set bit0 to activate carry in do-while loop
    sta $4016
    lsr a                 ; a has 0
    sta $4016

    :
        lda $4016,x
        and #3              ; famicom
        cmp #1              ; friendly reads
        rol pressed,x
    bcc :-

    lda pressed,x
    eor pressedLastFrame,x
    tay
    and pressed,x
    sta pressedNew,x
    tya
    eor pressedNew,x
    sta releasedNew,x
    
    rts
    
endfunc


; --------------------------------------------------------------------------------------------

.endscope

.endif

; --------------------------------------------------------------------------------------------
