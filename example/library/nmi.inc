; --------------------------------------------------------------------------------------------
; File nmi.inc
;
; 
;


.ifndef _LIB_NMI_
_LIB_NMI_ = 1


.include "config.h"
.include "header/ppu.h"
.include "header/funcCall.h"
.include "header/ndxdebug.h"

; timerTitle 0,"vblankTime"
; timerTitle 1,"PPUTransfer"

.scope ___NMIscope 

; --------------------------------------------------------------------------------------------
.pushseg
.segment "ZEROPAGE"

    frameCounter:   .res 1    ; count up every frame
    frameReady:     .res 1    ; if non-zero, main frame thread is done, NMI signals back by clearing it

.popseg

; --------------------------------------------------------------------------------------------
; Variables
; --------------------------------------------------------------------------------------------

.exportZP frameReady     
.exportZP frameCounter   

; --------------------------------------------------------------------------------------------
; Functions

.export NMIRoutine
declarefunc waitForNMI
declarefunc waitForXFrames, { frames x }

funcStartLibraryMode

func waitForNMI
    
    sec
    ror frameReady
    :
        bit frameReady
    bmi :-
    
    rts
    
endfunc

; --------------------------------------------------------------------------------------------

func waitForXFrames, { frames x }
    
    :
        txa
        pha
        call waitForNMI
        pla
        tax
    dex
    bne :-
    
    rts
    
endfunc

; --------------------------------------------------------------------------------------------


.proc NMIRoutine ; don't track with func
    
    inc frameCounter
    ;startTimer 0                ; monitor NMI time

    bit frameReady
    bpl notReady
    
        ; Main is ready, it doesn't expect CPU regs to be saved
        bit PPU_STATUS
        call ppu::updateSprites
       
        ; always check for generic buffer updates
        call ppu::transferVramBuffer
        
        ; updatePPUctrl -> this will get updated with the scroll update
        updatePPUmask
        
        jsr DoAlways
        
        ; NO PPU WRITES after here.
        ; -------------------------
        ;stopTimer 0
        clc
        ror frameReady
        
        rti

    notReady:
    
        pha ; back up registers (important) 
        txa  
        pha 
        tya 
        pha 
        
        ndxLuaExecStr "if (RAM.PPUmask % 32) > 4 then print('NMI: Main thread not ready!' ) end" ; Mod 32 > 4 indicates sprites or background are enabled

        jsr DoAlways

        pla                 ; restore  and exit 
        tay 
        pla 
        tax 
        pla 
        
        rti
        
    ; endif
    
    ; --------------------------------------------------------------------------------------------
    
    DoAlways:
    
		ldaPPUmaskBits #( MA_BACKVISIBLE | MA_SPRITESVISIBLE)
        beq :+
            ; SET SCROLL
            bit PPU_STATUS
            updateScroll 
            updatePPUnametable ; update CTRL
        :   
    rts
    
.endproc

funcEndLibraryMode

.endscope
.endif

