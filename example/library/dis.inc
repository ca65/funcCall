; --------------------------------------------------------------------------------------------
; File: dis.inc
;
; Disassembly of one instruction

.include "header/string.h"
.include "header/funcCall.h"

.scope _dis

declarefunc setAddress, { addr ax }
declarefunc disassemble, { stringPtr ax }, 1 ; skip first byte for parameters/locals

.pushseg
.segment "BSS"

    address:        .res 2
    
.segment "RODATA"

    ; 55 instructions plus "???"
   
    mnemonic_table:
    ;       0     1     2     3     4     5     6     7     8     9
    .byte "ADC","AND","ASL","BCC","BCS","BEQ","BIT","BMI","BNE","BPL" ; 0
    .byte "BRK","BVC","BVS","CLC","CLD","CLI","CLV","CMP","CPX","CPY" ; 1
    .byte "DEC","DEX","DEY","EOR","INC","INX","INY","JMP","JSR","LDA" ; 2
    .byte "LDX","LDY","LSR","NOP","ORA","PHA","PHP","PLA","PLP","ROL" ; 3
    .byte "ROR","RTI","RTS","SBC","SEC","SED","SEI","STA","STX","STY" ; 4
    .byte "TAX","TAY","TSX","TXA","TXS","TYA","???"                   ; 5
    
    ; --------------------------------------------------------------------------------------------
    ; info for decoding 256 possible bytes
    ; store size and mnemonic lookup
    .define instrByte(instrLen, mnemonic) .byte instrLen << 6 | mnemonic
    
    instruct_table:
    
    instrByte 1,10    ; 00 BRK 
    instrByte 2,34    ; 01 ORA (Indirect, X)
    instrByte 1,56    ; 02 
    instrByte 1,56    ; 03 
    instrByte 1,56    ; 04 
    instrByte 2,34    ; 05 ORA Zero Page
    instrByte 2,2     ; 06 ASL Zero Page
    instrByte 1,56    ; 07 
    instrByte 1,36    ; 08 PHP 
    instrByte 2,34    ; 09 ORA Immediate
    instrByte 1,2     ; 0A ASL Accumulator
    instrByte 1,56    ; 0B 
    instrByte 1,56    ; 0C 
    instrByte 3,34    ; 0D ORA Absolute
    instrByte 3,2     ; 0E ASL Absolute
    instrByte 1,56    ; 0F 
    instrByte 2,9     ; 10 BPL 
    instrByte 2,34    ; 11 ORA (Indirect), Y
    instrByte 1,56    ; 12 
    instrByte 1,56    ; 13 
    instrByte 1,56    ; 14 
    instrByte 2,34    ; 15 ORA Zero Page, X
    instrByte 2,2     ; 16 ASL Zero Page, X
    instrByte 1,56    ; 17 
    instrByte 1,13    ; 18 CLC 
    instrByte 3,34    ; 19 ORA Absolute, Y
    instrByte 1,56    ; 1A 
    instrByte 1,56    ; 1B 
    instrByte 1,56    ; 1C 
    instrByte 3,34    ; 1D ORA Absolute, X
    instrByte 3,2     ; 1E ASL Absolute, X
    instrByte 1,56    ; 1F 
    instrByte 3,28    ; 20 JSR 
    instrByte 2,1     ; 21 AND (Indirect, X)
    instrByte 1,56    ; 22 
    instrByte 1,56    ; 23 
    instrByte 2,6     ; 24 BIT Zero Page
    instrByte 2,1     ; 25 AND Zero Page
    instrByte 2,39    ; 26 ROL Zero Page
    instrByte 1,56    ; 27 
    instrByte 1,38    ; 28 PLP 
    instrByte 2,1     ; 29 AND Immediate
    instrByte 1,39    ; 2A ROL Accumulator
    instrByte 1,56    ; 2B 
    instrByte 3,6     ; 2C BIT Absolute
    instrByte 3,1     ; 2D AND Absolute
    instrByte 3,39    ; 2E ROL Absolute
    instrByte 1,56    ; 2F 
    instrByte 2,7     ; 30 BMI 
    instrByte 2,1     ; 31 AND (Indirect), Y
    instrByte 1,56    ; 32 
    instrByte 1,56    ; 33 
    instrByte 1,56    ; 34 
    instrByte 2,1     ; 35 AND Zero Page, X
    instrByte 2,39    ; 36 ROL Zero Page, X
    instrByte 1,56    ; 37 
    instrByte 1,44    ; 38 SEC 
    instrByte 3,1     ; 39 AND Absolute, Y
    instrByte 1,56    ; 3A 
    instrByte 1,56    ; 3B 
    instrByte 1,56    ; 3C 
    instrByte 3,1     ; 3D AND Absolute, X
    instrByte 3,39    ; 3E ROL Absolute, X
    instrByte 1,56    ; 3F 
    instrByte 1,41    ; 40 RTI 
    instrByte 2,23    ; 56 EOR (Indirect, X)
    instrByte 1,56    ; 41 
    instrByte 1,56    ; 42 
    instrByte 1,56    ; 43 
    instrByte 2,23    ; 44 EOR Zero Page
    instrByte 2,32    ; 45 LSR Zero Page
    instrByte 1,56    ; 46 
    instrByte 1,35    ; 47 PHA 
    instrByte 2,23    ; 48 EOR Immediate
    instrByte 1,32    ; 4A LSR Accumulator
    instrByte 1,56    ; 4B 
    instrByte 3,27    ; 4C JMP Absolute
    instrByte 3,23    ; 4D EOR Absolute
    instrByte 3,32    ; 4E LSR Absolute
    instrByte 1,56    ; 4F 
    instrByte 2,11    ; 49 BVC 
    instrByte 2,23    ; 50 EOR (Indirect), Y
    instrByte 1,56    ; 51 
    instrByte 1,56    ; 52 
    instrByte 1,56    ; 53 
    instrByte 2,23    ; 54 EOR Zero Page, X
    instrByte 2,32    ; 55 LSR Zero Page, X
    instrByte 1,56    ; 56 
    instrByte 1,15    ; 58 CLI 
    instrByte 3,23    ; 59 EOR Absolute, Y
    instrByte 1,56    ; 5A 
    instrByte 1,56    ; 5B 
    instrByte 1,56    ; 5C 
    instrByte 3,23    ; 5D EOR Absolute, X
    instrByte 3,32    ; 5E LSR Absolute, X
    instrByte 1,56    ; 5F 
    instrByte 1,42    ; 60 RTS 
    instrByte 2,0     ; 61 ADC (Indirect, X)
    instrByte 1,56    ; 62 
    instrByte 1,56    ; 63 
    instrByte 1,56    ; 64 
    instrByte 2,0     ; 65 ADC Zero Page
    instrByte 2,40    ; 66 ROR Zero Page
    instrByte 1,56    ; 67 
    instrByte 1,37    ; 68 PLA 
    instrByte 2,0     ; 69 ADC Immediate
    instrByte 1,40    ; 6A ROR Accumulator
    instrByte 1,56    ; 6B 
    instrByte 3,27    ; 6C JMP Indirect
    instrByte 3,0     ; 6D ADC Absolute
    instrByte 3,40    ; 6E ROR Absolute
    instrByte 1,56    ; 6F 
    instrByte 2,12    ; 70 BVS 
    instrByte 2,0     ; 71 ADC (Indirect), Y
    instrByte 1,56    ; 72 
    instrByte 1,56    ; 73 
    instrByte 1,56    ; 74 
    instrByte 2,0     ; 75 ADC Zero Page, X
    instrByte 2,40    ; 76 ROR Zero Page, X
    instrByte 1,56    ; 77 
    instrByte 1,46    ; 78 SEI 
    instrByte 3,0     ; 79 ADC Absolute, Y
    instrByte 1,56    ; 7A 
    instrByte 1,56    ; 7B 
    instrByte 1,56    ; 7C 
    instrByte 3,0     ; 7D ADC Absolute, X
    instrByte 3,40    ; 7E ROR Absolute, X
    instrByte 1,56    ; 7F 
    instrByte 1,56    ; 80 
    instrByte 2,47    ; 81 STA (Indirect, X)
    instrByte 1,56    ; 82 
    instrByte 1,56    ; 83 
    instrByte 2,49    ; 84 STY Zero Page
    instrByte 2,47    ; 85 STA Zero Page
    instrByte 2,48    ; 86 STX Zero Page
    instrByte 1,56    ; 87 
    instrByte 1,22    ; 88 DEY 
    instrByte 1,56    ; 89 
    instrByte 1,53    ; 8A TXA 
    instrByte 1,56    ; 8B 
    instrByte 3,49    ; 8C STY Absolute
    instrByte 3,47    ; 8D STA Absolute
    instrByte 3,48    ; 8E STX Absolute
    instrByte 1,56    ; 8F 
    instrByte 2,3     ; 90 BCC 
    instrByte 2,47    ; 91 STA (Indirect), Y
    instrByte 1,56    ; 92 
    instrByte 1,56    ; 93 
    instrByte 2,49    ; 94 STY Zero Page, X
    instrByte 2,47    ; 95 STA Zero Page, X
    instrByte 2,48    ; 96 STX Zero Page, Y
    instrByte 1,56    ; 97 
    instrByte 1,55    ; 98 TYA 
    instrByte 3,47    ; 99 STA Absolute, Y
    instrByte 1,54    ; 9A TXS 
    instrByte 1,56    ; 9B 
    instrByte 1,56    ; 9C 
    instrByte 3,47    ; 9D STA Absolute, X
    instrByte 1,56    ; 9E 
    instrByte 1,56    ; 9F 
    instrByte 2,31    ; A0 LDY Immediate
    instrByte 2,29    ; A1 LDA (Indirect, X)
    instrByte 2,30    ; A2 LDX Immediate
    instrByte 1,56    ; A3 
    instrByte 2,31    ; A4 LDY Zero Page
    instrByte 2,29    ; A5 LDA Zero Page
    instrByte 2,30    ; A6 LDX Zero Page
    instrByte 1,56    ; A7 
    instrByte 1,51    ; A8 TAY 
    instrByte 2,29    ; A9 LDA Immediate
    instrByte 1,50    ; AA TAX 
    instrByte 1,56    ; AB 
    instrByte 3,31    ; AC LDY Absolute
    instrByte 3,29    ; AD LDA Absolute
    instrByte 3,30    ; AE LDX Absolute
    instrByte 1,56    ; AF 
    instrByte 2,4     ; B0 BCS 
    instrByte 2,29    ; B1 LDA (Indirect), Y
    instrByte 1,56    ; B2 
    instrByte 1,56    ; B3 
    instrByte 2,31    ; B4 LDY Zero Page, X
    instrByte 2,29    ; B5 LDA Zero Page, X
    instrByte 2,30    ; B6 LDX Zero Page, Y
    instrByte 1,56    ; B7 
    instrByte 1,16    ; B8 CLV 
    instrByte 3,29    ; B9 LDA Absolute, Y
    instrByte 1,52    ; BA TSX 
    instrByte 1,56    ; BB 
    instrByte 3,31    ; BC LDY Absolute, X
    instrByte 3,29    ; BD LDA Absolute, X
    instrByte 3,30    ; BE LDX Absolute, Y
    instrByte 1,56    ; BF 
    instrByte 2,19    ; C0 CPY Immediate
    instrByte 2,17    ; C1 CMP (Indirect, X)
    instrByte 1,56    ; C2 
    instrByte 1,56    ; C3 
    instrByte 2,19    ; C4 CPY Zero Page
    instrByte 2,17    ; C5 CMP Zero Page
    instrByte 2,20    ; C6 DEC Zero Page
    instrByte 1,56    ; C7 
    instrByte 1,26    ; C8 INY 
    instrByte 2,17    ; C9 CMP Immediate
    instrByte 1,21    ; CA DEX 
    instrByte 1,56    ; CB 
    instrByte 3,19    ; CC CPY Absolute
    instrByte 3,17    ; CD CMP Absolute
    instrByte 3,20    ; CE DEC Absolute
    instrByte 1,56    ; CF 
    instrByte 2,8     ; D0 BNE 
    instrByte 2,17    ; D1 CMP (Indirect), Y
    instrByte 1,56    ; D2 
    instrByte 1,56    ; D3 
    instrByte 1,56    ; D4 
    instrByte 2,17    ; D5 CMP Zero Page, X
    instrByte 2,20    ; D6 DEC Zero Page, X
    instrByte 1,56    ; D7 
    instrByte 1,14    ; D8 CLD 
    instrByte 3,17    ; D9 CMP Absolute, Y
    instrByte 1,56    ; DA 
    instrByte 1,56    ; DB 
    instrByte 1,56    ; DC 
    instrByte 3,17    ; DD CMP Absolute, X
    instrByte 3,20    ; DE DEC Absolute, X
    instrByte 1,56    ; DF 
    instrByte 2,18    ; E0 CPX Immediate
    instrByte 2,43    ; E1 SBC (Indirect, X)
    instrByte 1,56    ; E2 
    instrByte 1,56    ; E3 
    instrByte 2,18    ; E4 CPX Zero Page
    instrByte 2,43    ; E5 SBC Zero Page
    instrByte 2,24    ; E6 INC Zero Page
    instrByte 1,56    ; E7 
    instrByte 1,25    ; E8 INX 
    instrByte 2,43    ; E9 SBC Immediate
    instrByte 1,33    ; EA NOP 
    instrByte 1,56    ; EB 
    instrByte 3,18    ; EC CPX Absolute
    instrByte 3,43    ; ED SBC Absolute
    instrByte 3,24    ; EE INC Absolute
    instrByte 1,56    ; EF 
    instrByte 2,5     ; F0 BEQ 
    instrByte 2,43    ; F1 SBC (Indirect), Y
    instrByte 1,56    ; F2 
    instrByte 1,56    ; F3 
    instrByte 1,56    ; F4 
    instrByte 2,43    ; F5 SBC Zero Page, X
    instrByte 2,24    ; F6 INC Zero Page, X
    instrByte 1,56    ; F7 
    instrByte 1,45    ; F8 SED 
    instrByte 3,43    ; F9 SBC Absolute, Y
    instrByte 1,56    ; FA 
    instrByte 1,56    ; FB 
    instrByte 1,56    ; FC 
    instrByte 3,43    ; FD SBC Absolute, X
    instrByte 3,24    ; FE INC Absolute, X
    instrByte 1,56    ; FF
    
    ; need 4 bits for mode -> macro to store two mode values per byte
    secondNib   .set 0
    .macro modeByte mode
        .if secondNib
            .byte mode << 4 | firstNib
            secondNib .set 0
        .else
            firstNib .set mode
            secondNib .set 1
        .endif
    .endmacro
    
    mode_table:
    
    modeByte 0    ; 00 BRK 
    modeByte 4    ; 01 ORA (Indirect, X)
    modeByte 0    ; 02 
    modeByte 0    ; 03 
    modeByte 0    ; 04 
    modeByte 0    ; 05 ORA Zero Page
    modeByte 0    ; 06 ASL Zero Page
    modeByte 0    ; 07 
    modeByte 0    ; 08 PHP 
    modeByte 1    ; 09 ORA Immediate
    modeByte 7    ; 0A ASL Accumulator
    modeByte 0    ; 0B 
    modeByte 0    ; 0C 
    modeByte 0    ; 0D ORA Absolute
    modeByte 0    ; 0E ASL Absolute
    modeByte 0    ; 0F 
    modeByte 8    ; 10 BPL 
    modeByte 5    ; 11 ORA (Indirect), Y
    modeByte 0    ; 12 
    modeByte 0    ; 13 
    modeByte 0    ; 14 
    modeByte 2    ; 15 ORA Zero Page, X
    modeByte 2    ; 16 ASL Zero Page, X
    modeByte 0    ; 17 
    modeByte 0    ; 18 CLC 
    modeByte 3    ; 19 ORA Absolute, Y
    modeByte 0    ; 1A 
    modeByte 0    ; 1B 
    modeByte 0    ; 1C 
    modeByte 2    ; 1D ORA Absolute, X
    modeByte 2    ; 1E ASL Absolute, X
    modeByte 0    ; 1F 
    modeByte 0    ; 20 JSR 
    modeByte 4    ; 21 AND (Indirect, X)
    modeByte 0    ; 22 
    modeByte 0    ; 23 
    modeByte 0    ; 24 BIT Zero Page
    modeByte 0    ; 25 AND Zero Page
    modeByte 0    ; 26 ROL Zero Page
    modeByte 0    ; 27 
    modeByte 0    ; 28 PLP 
    modeByte 1    ; 29 AND Immediate
    modeByte 7    ; 2A ROL Accumulator
    modeByte 0    ; 2B 
    modeByte 0    ; 2C BIT Absolute
    modeByte 0    ; 2D AND Absolute
    modeByte 0    ; 2E ROL Absolute
    modeByte 0    ; 2F 
    modeByte 8    ; 30 BMI 
    modeByte 5    ; 31 AND (Indirect), Y
    modeByte 0    ; 32 
    modeByte 0    ; 33 
    modeByte 0    ; 34 
    modeByte 2    ; 35 AND Zero Page, X
    modeByte 2    ; 36 ROL Zero Page, X
    modeByte 0    ; 37 
    modeByte 0    ; 38 SEC 
    modeByte 3    ; 39 AND Absolute, Y
    modeByte 0    ; 3A 
    modeByte 0    ; 3B 
    modeByte 0    ; 3C 
    modeByte 2    ; 3D AND Absolute, X
    modeByte 2    ; 3E ROL Absolute, X
    modeByte 0    ; 3F 
    modeByte 0    ; 40 RTI 
    modeByte 4    ; 56 EOR (Indirect, X)
    modeByte 0    ; 41 
    modeByte 0    ; 42 
    modeByte 0    ; 43 
    modeByte 0    ; 44 EOR Zero Page
    modeByte 0    ; 45 LSR Zero Page
    modeByte 0    ; 46 
    modeByte 0    ; 47 PHA 
    modeByte 1    ; 48 EOR Immediate
    modeByte 7    ; 4A LSR Accumulator
    modeByte 0    ; 4B 
    modeByte 0    ; 4C JMP Absolute
    modeByte 0    ; 4D EOR Absolute
    modeByte 0    ; 4E LSR Absolute
    modeByte 0    ; 4F 
    modeByte 8    ; 49 BVC 
    modeByte 5    ; 50 EOR (Indirect), Y
    modeByte 0    ; 51 
    modeByte 0    ; 52 
    modeByte 0    ; 53 
    modeByte 2    ; 54 EOR Zero Page, X
    modeByte 2    ; 55 LSR Zero Page, X
    modeByte 0    ; 56 
    modeByte 0    ; 58 CLI 
    modeByte 3    ; 59 EOR Absolute, Y
    modeByte 0    ; 5A 
    modeByte 0    ; 5B 
    modeByte 0    ; 5C 
    modeByte 2    ; 5D EOR Absolute, X
    modeByte 2    ; 5E LSR Absolute, X
    modeByte 0    ; 5F 
    modeByte 0    ; 60 RTS 
    modeByte 4    ; 61 ADC (Indirect, X)
    modeByte 0    ; 62 
    modeByte 0    ; 63 
    modeByte 0    ; 64 
    modeByte 0    ; 65 ADC Zero Page
    modeByte 0    ; 66 ROR Zero Page
    modeByte 0    ; 67 
    modeByte 0    ; 68 PLA 
    modeByte 1    ; 69 ADC Immediate
    modeByte 7    ; 6A ROR Accumulator
    modeByte 0    ; 6B 
    modeByte 6    ; 6C JMP Indirect
    modeByte 0    ; 6D ADC Absolute
    modeByte 0    ; 6E ROR Absolute
    modeByte 0    ; 6F 
    modeByte 8    ; 70 BVS 
    modeByte 5    ; 71 ADC (Indirect), Y
    modeByte 0    ; 72 
    modeByte 0    ; 73 
    modeByte 0    ; 74 
    modeByte 2    ; 75 ADC Zero Page, X
    modeByte 2    ; 76 ROR Zero Page, X
    modeByte 0    ; 77 
    modeByte 0    ; 78 SEI 
    modeByte 3    ; 79 ADC Absolute, Y
    modeByte 0    ; 7A 
    modeByte 0    ; 7B 
    modeByte 0    ; 7C 
    modeByte 2    ; 7D ADC Absolute, X
    modeByte 2    ; 7E ROR Absolute, X
    modeByte 0    ; 7F 
    modeByte 0    ; 80 
    modeByte 4    ; 81 STA (Indirect, X)
    modeByte 0    ; 82 
    modeByte 0    ; 83 
    modeByte 0    ; 84 STY Zero Page
    modeByte 0    ; 85 STA Zero Page
    modeByte 0    ; 86 STX Zero Page
    modeByte 0    ; 87 
    modeByte 0    ; 88 DEY 
    modeByte 0    ; 89 
    modeByte 0    ; 8A TXA 
    modeByte 0    ; 8B 
    modeByte 0    ; 8C STY Absolute
    modeByte 0    ; 8D STA Absolute
    modeByte 0    ; 8E STX Absolute
    modeByte 0    ; 8F 
    modeByte 8    ; 90 BCC 
    modeByte 5    ; 91 STA (Indirect), Y
    modeByte 0    ; 92 
    modeByte 0    ; 93 
    modeByte 2    ; 94 STY Zero Page, X
    modeByte 2    ; 95 STA Zero Page, X
    modeByte 3    ; 96 STX Zero Page, Y
    modeByte 0    ; 97 
    modeByte 0    ; 98 TYA 
    modeByte 3    ; 99 STA Absolute, Y
    modeByte 0    ; 9A TXS 
    modeByte 0    ; 9B 
    modeByte 0    ; 9C 
    modeByte 2    ; 9D STA Absolute, X
    modeByte 0    ; 9E 
    modeByte 0    ; 9F 
    modeByte 1    ; A0 LDY Immediate
    modeByte 4    ; A1 LDA (Indirect, X)
    modeByte 1    ; A2 LDX Immediate
    modeByte 0    ; A3 
    modeByte 0    ; A4 LDY Zero Page
    modeByte 0    ; A5 LDA Zero Page
    modeByte 0    ; A6 LDX Zero Page
    modeByte 0    ; A7 
    modeByte 0    ; A8 TAY 
    modeByte 1    ; A9 LDA Immediate
    modeByte 0    ; AA TAX 
    modeByte 0    ; AB 
    modeByte 0    ; AC LDY Absolute
    modeByte 0    ; AD LDA Absolute
    modeByte 0    ; AE LDX Absolute
    modeByte 0    ; AF 
    modeByte 8    ; B0 BCS 
    modeByte 5    ; B1 LDA (Indirect), Y
    modeByte 0    ; B2 
    modeByte 0    ; B3 
    modeByte 2    ; B4 LDY Zero Page, X
    modeByte 2    ; B5 LDA Zero Page, X
    modeByte 3    ; B6 LDX Zero Page, Y
    modeByte 0    ; B7 
    modeByte 0    ; B8 CLV 
    modeByte 3    ; B9 LDA Absolute, Y
    modeByte 0    ; BA TSX 
    modeByte 0    ; BB 
    modeByte 2    ; BC LDY Absolute, X
    modeByte 2    ; BD LDA Absolute, X
    modeByte 3    ; BE LDX Absolute, Y
    modeByte 0    ; BF 
    modeByte 1    ; C0 CPY Immediate
    modeByte 4    ; C1 CMP (Indirect, X)
    modeByte 0    ; C2 
    modeByte 0    ; C3 
    modeByte 0    ; C4 CPY Zero Page
    modeByte 0    ; C5 CMP Zero Page
    modeByte 0    ; C6 DEC Zero Page
    modeByte 0    ; C7 
    modeByte 0    ; C8 INY 
    modeByte 1    ; C9 CMP Immediate
    modeByte 0    ; CA DEX 
    modeByte 0    ; CB 
    modeByte 0    ; CC CPY Absolute
    modeByte 0    ; CD CMP Absolute
    modeByte 0    ; CE DEC Absolute
    modeByte 0    ; CF 
    modeByte 8    ; D0 BNE 
    modeByte 5    ; D1 CMP (Indirect), Y
    modeByte 0    ; D2 
    modeByte 0    ; D3 
    modeByte 0    ; D4 
    modeByte 2    ; D5 CMP Zero Page, X
    modeByte 2    ; D6 DEC Zero Page, X
    modeByte 0    ; D7 
    modeByte 0    ; D8 CLD 
    modeByte 3    ; D9 CMP Absolute, Y
    modeByte 0    ; DA 
    modeByte 0    ; DB 
    modeByte 0    ; DC 
    modeByte 2    ; DD CMP Absolute, X
    modeByte 2    ; DE DEC Absolute, X
    modeByte 0    ; DF 
    modeByte 1    ; E0 CPX Immediate
    modeByte 4    ; E1 SBC (Indirect, X)
    modeByte 0    ; E2 
    modeByte 0    ; E3 
    modeByte 0    ; E4 CPX Zero Page
    modeByte 0    ; E5 SBC Zero Page
    modeByte 0    ; E6 INC Zero Page
    modeByte 0    ; E7 
    modeByte 0    ; E8 INX 
    modeByte 1    ; E9 SBC Immediate
    modeByte 0    ; EA NOP 
    modeByte 0    ; EB 
    modeByte 0    ; EC CPX Absolute
    modeByte 0    ; ED SBC Absolute
    modeByte 0    ; EE INC Absolute
    modeByte 0    ; EF 
    modeByte 8    ; F0 BEQ 
    modeByte 5    ; F1 SBC (Indirect), Y
    modeByte 0    ; F2 
    modeByte 0    ; F3 
    modeByte 0    ; F4 
    modeByte 2    ; F5 SBC Zero Page, X
    modeByte 2    ; F6 INC Zero Page, X
    modeByte 0    ; F7 
    modeByte 0    ; F8 SED 
    modeByte 3    ; F9 SBC Absolute, Y
    modeByte 0    ; FA 
    modeByte 0    ; FB 
    modeByte 0    ; FC 
    modeByte 2    ; FD SBC Absolute, X
    modeByte 2    ; FE INC Absolute, X
    modeByte 0    ; FF 
    
.popseg

; --------------------------------------------------------------------------------------------
; Function: setAddress
;
; Set the address for where to begin disassembly

func setAddress, { addr ax }
    sta address
    stx address + 1
    rts
endfunc

; --------------------------------------------------------------------------------------------
; Function: disassemble
;
; -Disassemble an instruction starting at 'address'.
; -Store string results in string at address from registers ax.
; -Increment address when done.
; -Return value of opcode disassembled in reg. a

func disassemble, { stringPtr ax }

locals
    strPtr  .addr
    codePtr .addr
    op1     .byte
    op2     .byte
    op3     .byte
    size    .byte
    mode    .byte
    mnemonic .byte
endlocals

    sta local strPtr
    stx local strPtr + 1
    
    thisString = local strPtr
    
    lda address
    ldx address + 1
    sta local codePtr
    stx local codePtr + 1
    
    thisAddress = local codePtr
    temp = local mnemonic

    
    ; --------------------------------------------------------------------------------------------
    ; first, convert address to text
    ldy #$00                   ; index into string
    call str::byteToHex, { x } ; ASCII values returned in a, x ( on call: reg x holds 'address + 1')
    jsr appendAXtoString
    
    call str::byteToHex, { address } ; ASCII values returned in a, x
    jsr appendAXtoString
    
    ; --------------------------------------------------------------------------------------------  
    ; get mnemonic index, instruction, size and addressing mode
    
    ldy #$00
    lda (thisAddress), y    ; get opcode
    sta local op1
    tax
    lda instruct_table, x
    sta local mnemonic      ; save this
    rol                     ; move 2 msb to lsb
    rol
    rol
    and #$03
    sta local size
    
    ; load and save more bytes in case they are needed (this is easier then trying to retrieve them later?)
    
    iny
    lda (thisAddress), y    ; get possible operand
    sta local op2
    iny
    lda (thisAddress), y    ; get another possible operand
    sta local op3
    
    txa             ; load a with index/opcode again, but divide by two for 128 byte "mode" table..
    clc
    ror             ; ..but save lsb in carry
    tax
    lda mode_table, x
    bcc :+
        ; carry set, so use high nib
        lsr
        lsr
        lsr
        lsr
    :
    and #$0F            ; only need 4 bits for mode
    sta local mode
    ; finish mnemonic
    lda local mnemonic
    and #%00111111      ; 6 bits for opcode lookup
    sta local mnemonic
    asl
    clc
    adc local mnemonic  ; a * 3 for opcode lookup 
    tax                 ; x has index to mnemonic table
    
    ldy #$04            ; add space to string
    lda #' '        
    jsr appendAtoString
    
    ; --------------------------------------------------------------------------------------------
    ; copy three bytes for mnemonic
    :
        lda mnemonic_table, x
        jsr appendAtoString         ; increments y
        inx
    cpy #$08
    bne :-
    
    ; --------------------------------------------------------------------------------------------
    ; check size and mode 
    ; modes 0=implied, 1=immediate, 2=index x, 3=index y, 4=(indirect, x), 5=(indirect), y, 6=(indirect), 7=accumulator, 8=relative
    
    ldx local size           ; if one byte (instruction only), exit
    dex
    bne :+
        jmp done
    :
        ; add space
        lda #' '        
        jsr appendAtoString
        
        ; any prefix ?
        ldx local mode
        dex
        bne :+
            ; if mode = 1:
            lda #'#'
            jsr appendAtoString
            bne skip ; bra
        :
        ; if mode is 4, 5, 6 then add "(" to string (x off by 1)
        cpx #$03
        beq :+
        cpx #$04
        beq :+
        cpx #$05 
        bne :++
        :
            ; add "("
            lda #'('
            jsr appendAtoString
        :
        skip:
        
        lda #'$'
        jsr appendAtoString
        
        ; --------------------------------------------------------------------------------------------
        ; check for relative mode 8

        cpx #$07 ; x off by 1
        bne :+++
            ; change op2 and op3 to a 16 bit address to branch to
            ldx #$00            ; default high byte to add - for positive branch
            clc
            lda local op2       ; get branch amount
            bpl :+
                dex             ; extend bit 7 for negative branch
            :
            stx temp
            
            adc thisAddress
            sta local op2
            lda thisAddress + 1
            adc temp
            sta local op3
            
            ; add 2 for branch starting location
            lda local op2
            clc
            adc #$02
            sta local op2
            bcc :+
            inc local op3
            :
            jmp do3bytes
            
        :
        ; --------------------------------------------------------------------------------------------
        ; output any operands
        
        lda local size
        cmp #$03
        bne :+
            do3bytes:
            ; three byte instruction:
            call str::byteToHex, { local op3 }
            jsr appendAXtoString
            
        :   ; skip here for 2 byte instruction:    
            call str::byteToHex, { local op2 }
            jsr appendAXtoString
            
        ; --------------------------------------------------------------------------------------------    
        ; any suffix?        
        
        ldx local mode
        cpx #$02
        beq :+
        cpx #$04
        bne :++
        :
            ; ,X
            lda #','
            jsr appendAtoString
            lda #'X'
            jsr appendAtoString
            cpx #$04
            beq addbracket      ; ,X)
            bne done
        :
        cpx #$03
        bne :+
            ; ,Y
            addY:
            lda #','
            ldx #'Y'
            jsr appendAXtoString
            bne done
        :
        cpx #$05
        bne :+
            ; ),Y
            lda #')'
            jsr appendAtoString
            bne addY
        :
        cpx #$06
        bne :+
            ; )
            addbracket:
            lda #')'
            jsr appendAtoString
        :
        
    ; --------------------------------------------------------------------------------------------
    
    done:
    
    ; increment address
    lda address
    clc
    adc local size
    sta address
    bcc :+
    inc address + 1
    :
  
    ; zero terminate string
    lda #$00
    sta (thisString), y
    lda local op1           ; return opcode processed
    rts
    
    appendAXtoString:
    sta (thisString), y
    iny
    txa
    appendAtoString:
    sta (thisString), y
    iny
    rts
    brk ; mark end of routine for when disassembling
endfunc


.endscope
