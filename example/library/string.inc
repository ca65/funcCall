
.ifndef _STR_IO_LIB_
_STR_IO_LIB_ =1


.include "header/funcCall.h"
.include "header/ppu.h"


.scope stringScope

    declarefunc print, { _x .byte, _y  a, string xy }, 1 ; skip first byte for parameters/locals
    declarefunc byteToHex, { value a }

; --------------------------------------------------------------------------------------------
; Function: byteToHex
; convert the value in reg.a to hex ASCII characters, returned in order ax

func byteToHex, { value a }

    pha             ; save a (could use y, but nice to leave y alone too)
    and #$0F        ; keep low nibble only
    jsr get_digit   ; get low nibble
    tax             ; save it here
    pla             ; put byte in A again
    lsr
    lsr
    lsr
    lsr
    ; fall through, get second digit
    get_digit:
    
    cmp #10
    bcc :+
        adc #$36            ; add #$37, but carry is set, so add one less
        rts
    :
    adc #$30                ; value in ASCII for 0 - 9
    rts  
    
endfunc

; --------------------------------------------------------------------------------------------
; Function: print
; Copies a zero terminated string and coordinates to the buffer for 
; writing to PPU vram via <ppu::transferVramBuffer>
; For now, functionality is limited to the first nametable

func print, { _x .byte, _y  a, string xy }

locals
    strPtr      .addr
    nameaddr    .addr
endlocals

    namelo = local nameaddr
    namehi = local nameaddr + 1
    
    ; create string pointer
    stx local strPtr
    sty local strPtr + 1
    
    ; calculate nametable location
    ; nametable address = $2000 + $20 * Y + X
    asl     ; * 2
    asl     ; shift to high nib
    asl
    rol
    rol namehi
    rol 
    rol namehi
    and #$F0
    clc            
    adc param _x
    sta namelo
    lda namehi
    and #$03
    adc #$20
    sta namehi
    
    ; count length
    ldy #$FF
    :   
        iny               
        lda (local strPtr), y
    bne :-
    
    call ppu::loadVRAMBufWithAddr, { local strPtr , y, local nameaddr }
    rts
    
endfunc

.endscope

.endif
