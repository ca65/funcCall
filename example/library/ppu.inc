; --------------------------------------------------------------------------------------------
; Files: ppu.inc
; Implementation for some PPU functions
;

 
.ifndef ___PPU
___PPU = 1

.include "config.h"
.include "header/funcCall.h"
.include "header/nmi.h"
.include "header/ndxdebug.h"

.ifndef ::__VRAM_BUFFER_SIZE__
    ::__VRAM_BUFFER_SIZE__    = 192
.endif

; *****************************************************************************
.scope PPU_lib

.pushseg
.segment "ZEROPAGE"

    PPUctrl:                .res 1
    PPUmask:                .res 1
    PPUnametable:           .res 1
    PPUxScroll:             .res 1
    PPUyScroll:             .res 1
    dataLength:             .res 1    ; for ppu update
    
.segment "BSS"

    vramBufferIndex:        .res 1
    vramBuffer:             .res ::__VRAM_BUFFER_SIZE__ 
    paletteShadow:          .res 32

.popseg

; --------------------------------------------------------------------------------------------
; Variables
; --------------------------------------------------------------------------------------------

.exportZP PPUctrl           
.exportZP PPUmask        
.exportZP PPUnametable 
.exportZP PPUxScroll      
.exportZP PPUyScroll   

.export paletteShadow
.export backgroundPalette   = paletteShadow         
.export spritePalette       = paletteShadow + 16
   

.export vramBufferIndex
.export vramBuffer        
 

; --------------------------------------------------------------------------------------------
; Functions:
; -----------------------------------------------------------------------------------------

funcStartLibraryMode

declarefunc transferVramBuffer
declarefunc loadVRAMBufWithAddr, {dataSourcePtr ax , length y , destAddressPPU .word }
declarefunc loadVRAMBuf, { dataSourcePtr ax }
declarefunc clearNameTable, { nt x, tile a, attrib y }
declarefunc updateBgPalette
declarefunc setBgPalette, { pal ax }
declarefunc updateSprites


; --------------------------------------------------------------------------------------------
;
; Function: transferVramBuffer
;
;   Parameters:
;   none
;
;   Intended to be called as soon as possible in the NMI routine.
;   This will send data to the PPU in a partially unrolled loop.
; 
;   Data Format:
;
;   Data in bytes:
;   PPUAddressHI, PPUAddressLO, datalength (max 32), data, data, ..
;
;   The datalength is the number of bytes to be sent, less 1, so one byte minimal will be written on datalength of 0.
;   Multiple blocks of formatted data can be sent.
;
;    Note:
;    For PPUAddressHI, bit 6 set means use column mode. Bit 7 set means no more data blocks (done, exit).
;    Therefore, the end of the data in the buffer should be marked with a byte of value $80 or higher.

func transferVramBuffer
    
    ;startTimer 1  
 
    ldy #0                      ; y is index into buffer
    sty vramBufferIndex         ; reset index to zero so the main thread can use a fresh buffer
    jmp Start                   ; start at bottom of the loop
 
    loopPPUwrite:
        ; reg x now holds the first byte from the buffer:
 
        lda PPUctrl ; load current control bits
 
        ; check for vertical write (bit 6)
        ; bit 7 cannot be set here, so we can use CMP
       
        cpx #%01000000
        bcc notVertical        
            ora #CT_ADDRINC32
        notVertical:
       
        sta PPU_CTRL
       
        ; write high byte of PPU address:
        stx PPU_ADDRESS                    ; bit6 ignored here.
       
        tya                                ; count index with reg A
        ldx vramBuffer + 1, y              ; PPU address low
        stx PPU_ADDRESS
       
        ; load count:
        ldx vramBuffer + 2, y
 
        ; save count here:
        ; this will be shifted to the right to check bits 0 through 4
        ; and write to PPU for each power
 
        stx dataLength
 
        ; count is actually less 1, so 0 means 1 byte, 31 means 32 bytes
        ; so always do 1 write at least:
 
        ldx vramBuffer + 3, y
        stx PPU_DATA
        clc 
        adc #3
        tay
 
        lsr dataLength
        bcc :+
            ldx vramBuffer + 1, y
            stx PPU_DATA
           
            ; we need to only add one to the offset, but still track in A:
            iny
            tya        
        :
 
        lsr dataLength
        bcc :+
            ldx vramBuffer + 1, y
            stx PPU_DATA
            ldx vramBuffer + 2, y
            stx PPU_DATA
           
            ; add 2 to the offset:
           
            adc #1 ; add 2, C is set
            tay
        :
       
        lsr dataLength
        bcc :+
            ldx vramBuffer + 1, y
            stx PPU_DATA
            ldx vramBuffer + 2, y
            stx PPU_DATA
            ldx vramBuffer + 3, y
            stx PPU_DATA
            ldx vramBuffer + 4, y
            stx PPU_DATA
           
            ; add 4 to the offset:
           
            adc #3 ; C is set
            tay
        :
 
        lsr dataLength
        bcc :+
            ldx vramBuffer + 1, y
            stx PPU_DATA
            ldx vramBuffer + 2, y
            stx PPU_DATA
            ldx vramBuffer + 3, y
            stx PPU_DATA
            ldx vramBuffer + 4, y
            stx PPU_DATA
            ldx vramBuffer + 5, y
            stx PPU_DATA
            ldx vramBuffer + 6, y
            stx PPU_DATA
            ldx vramBuffer + 7, y
            stx PPU_DATA
            ldx vramBuffer + 8, y
            stx PPU_DATA
           
            ; add 8 to the offset:
           
            adc #7 ; C is set
            tay
        :
 
        lsr dataLength
        bcc :+
            ldx vramBuffer + 1, y
            stx PPU_DATA
            ldx vramBuffer + 2, y
            stx PPU_DATA
            ldx vramBuffer + 3, y
            stx PPU_DATA
            ldx vramBuffer + 4, y
            stx PPU_DATA
            ldx vramBuffer + 5, y
            stx PPU_DATA
            ldx vramBuffer + 6, y
            stx PPU_DATA
            ldx vramBuffer + 7, y
            stx PPU_DATA
            ldx vramBuffer + 8, y
            stx PPU_DATA
            ldx vramBuffer + 9, y
            stx PPU_DATA
            ldx vramBuffer + 10, y
            stx PPU_DATA
            ldx vramBuffer + 11, y
            stx PPU_DATA
            ldx vramBuffer + 12, y
            stx PPU_DATA
            ldx vramBuffer + 13, y
            stx PPU_DATA
            ldx vramBuffer + 14, y
            stx PPU_DATA
            ldx vramBuffer + 15, y
            stx PPU_DATA
            ldx vramBuffer + 16, y
            stx PPU_DATA
           
            ; add 16 to the offset:
           
            adc #15 ; C is set
            tay
        :
        iny
    Start:
 
    ; if bit 7 is set, exit loop:
 
    ldx vramBuffer, y
    bmi done
    jmp loopPPUwrite
 
    done:
   
    ; mark buffer as empty (reg x has N bit set)
    ; first byte of buffer will be negative, if this buffer used now, nothing happens:
  ;  stopTimer 1
    stx vramBuffer              
    rts

endfunc

; --------------------------------------------------------------------------------------------
;
; Function: loadVRAMBuf
;
;   Parameters:
;
;   dataSourcePtr - The address of the data to be copied.

;   Note:
;
;   The data byte in the data sent to the buffer (the third byte) must be 1 less than the length of the PPU data.

func loadVRAMBuf, { dataSourcePtr ax }

    locals 2
        ptr .addr
    endlocals
    
    ; reminder, format for buffer data: ppuHi, ppuLow, datasize - 1, data, data, data
    ;                                      \__ vramBufferIndex
    
    sta local ptr
    stx local ptr + 1
    
    ldy #$02                ; set index to 2 to get size
    lda (local ptr), y      ; load size of data (off by one)
    clc
    adc #03                 ; add 3 more for ppuhi, ppulo, datasize byte 
    tay
    
    adc vramBufferIndex     
    tax
    inx                     ; x now indexes to the next free location in the buffer
    
    ; write $FF since we are at the end of the buffer
    lda #$FF
    sta vramBuffer, x
    stx vramBufferIndex
    
    : ; count back from end of string
        dex
        lda (local ptr), y
        sta vramBuffer, x
    dey
    bpl :-

    rts
   
endfunc

; --------------------------------------------------------------------------------------------
; Function: loadVRAMBufWithAddr
;
;   Parameters:
;
;   dataSourcePtr - Address of the source of the data. 
;   length - The number of bytes to be sent to the PPU.
;   destAddressPPU - The address in VRAM where the data is to be sent.
;

func loadVRAMBufWithAddr, {dataSourcePtr ax , length y , destAddressPPU .addr }

; write data to PPU buffer
; Send a pointer for source data, length and where in the PPU data is going.
    
    locals 2
        srcPtr  .addr
    endlocals
    
    ; save the pointer:
    sta local srcPtr
    stx local srcPtr + 1
    dey  ; save length as one less
    tya
    pha     ; save a on stack
    
        ; length sent only is data, need to add dest address (2) and length(1)
        clc
        adc vramBufferIndex
        adc #$04 ; add 3 bytes to length, plus one more since we decremented y
    
        ; write FF to end of buffer now since we are at the end of the buffer
        tax
    
        lda #$FF
        sta vramBuffer, x
        stx vramBufferIndex
        
        : ; count back from end of string
            dex
            lda (local srcPtr), y 
            sta vramBuffer, x
        dey
        bpl :-
    
    pla ; retrieve the adjusted length byte
    
    sta vramBuffer - 1, x
    lda param destAddressPPU + 0
    sta vramBuffer - 2, x
    lda param destAddressPPU + 1
    sta vramBuffer - 3, x
    
    rts
    
endfunc

; --------------------------------------------------------------------------------------------
; copy the palette memory to the PPU

func updateBgPalette

    lda  #>PAL_ADDRESS
    sta PPU_ADDRESS
    lda  #<PAL_ADDRESS
    sta PPU_ADDRESS
    
    ldx #$00
    :
        lda paletteShadow, x
        sta PPU_DATA
    inx
    cpx #16
    bcc :-
    
    rts
    
endfunc

; --------------------------------------------------------------------------------------------
; Function: clearNameTable
; high byte of nametable in x, tile to write in y, attribute table value in a
; do not call unless PPU rendering is OFF, it is safe to leave NMI on


func clearNameTable, { nt x, tile a, attrib y }

    ; save attribute value          
    ; set nametable
    stx PPU_ADDRESS
    ldx #$00
    stx PPU_ADDRESS
    
    ; 240 * 4 = 960 bytes nametable
    ldx #240
    :     
        sta PPU_DATA
        sta PPU_DATA
        sta PPU_DATA
        sta PPU_DATA
    dex
    bne :- 
    
    ; write $40 bytes for attributes:
    ldx #$40
    :
        sty PPU_DATA
    dex
    bne :-
    
    rts

endfunc

; ----------------------------------------------------------------------------------------------
; Function: setBgPalette
;
;   Parameters:
;
;   pal - the address of the data to be copied to the background palette.
;
;   This will copy the data to the shadow register <ppu::backgroundPalette>

func setBgPalette, { pal ax }

    locals 2
        palPtr  .addr
    endlocals
    
    sta local palPtr
    stx local palPtr + 1
    
    ldy #15
    :
        lda (local palPtr),y
        sta backgroundPalette, y
    dey
    bpl :-
    
    rts
    
endfunc

; --------------------------------------------------------------------------------------------

func updateSprites
        lda #$00
        sta $2003
        lda #$02
        sta $4014
        rts
endfunc

funcEndLibraryMode

.endscope

.endif

; ----------------------------------------------------------------------------------------
