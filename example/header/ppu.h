; Section: ppu.h
;   
; Variables and routines to interact with the NES PPU. 
;
.ifndef _PPU_H_
_PPU_H_ = 1

.include "header/funcCall.h"
.include "header/nes_constants.h"

.scope ppu

funcStartLibraryMode 

    .importzp   PPUctrl
    .importzp   PPUmask        
    .importzp   PPUnametable     
    .importzp   PPUxScroll    
    .importzp   PPUyScroll
    .import     vramBufferIndex
    .import     vramBuffer
    .import     paletteShadow
    .import backgroundPalette
    .import spritePalette
    
; --------------------------------------------------------------------------------------------



    declarefunc loadVRAMBuf, { dataSourcePtr ax }    
    declarefunc setBgPalette, { pal ax } 
    declarefunc clearNameTable, { nt x , tile a, attrib y }
    declarefunc updateBgPalette
    declarefunc transferVramBuffer
    declarefunc loadVRAMBufWithAddr, {dataSourcePtr ax, length y, destAddressPPU .word }
    declarefunc updateSprites    

; --------------------------------------------------------------------------------------------

funcEndLibraryMode 

; --------------------------------------------------------------------------------------------

.endscope

; --------------------------------------------------------------------------------------------
; Function: ldaPPUctrlBits
;
; Parameters:
;
;   mask - Optional - Bits to mask to 0.
;
;   This macro will load the current value of <ppu::PPUctrl> into register A
;
;   Example: Load the current NMI enable bit into A:
; > ldaPPUctrlBits #CT_NMI
;
;   It can also be used in HL macros:
;
; > if not ldaPPUctrlBits #CT_NMI
; >     staPPUctrlBits #CT_NMI, now
; > endif
;
;   See Also:
;   <staPPUctrlBits>

.macro ldaPPUctrlBits mask
    
    lda ppu::PPUctrl
    .ifnblank mask
        and mask
    .endif
  
.endmacro

; --------------------------------------------------------------------------------------------
; Function: staPPUctrlBits
;
; Parameters:
;
;   setbits - Optional - Bits to set.
;   update - Optional - any value.
;
;   This macro will store the current value of register A to <ppu::PPUctrl>.
;   If setbits is sent, it will OR setbits with register A before storing to <ppu::PPUctrl>.
;   If update is sent (non-blank), register A will also be stored immediately to the PPU CTRL port.
;
;   Example: Enable NMI requests from the PPU:
; > ldaPPUctrlBits
; > staPPUctrlBits #CT_NMI, now ; save to shadow register, and the PPU.
;
;   See Also:
;   <ldaPPUctrlBits>

.macro staPPUctrlBits setbits,update
    
    .ifnblank setbits
        ora setbits
    .endif
    
    sta ppu::PPUctrl
   
    .ifnblank update
        sta PPU_CTRL
    .endif
.endmacro

; --------------------------------------------------------------------------------------------
; Function: ldaPPUmaskBits
;
; Parameters:
;
;   mask - Optional - Bits to mask to 0.
;
;   This macro will load the current value of <ppu::PPUmask> into register A
;
;   Example: Load the current background enable bit into A:
; > ldaPPUmaskBits #MA_BACKVISIBLE
;
;   It can also be used in HL macros:
;
; > if not ldaPPUmaskBits ##MA_BACKVISIBLE
; >     staPPUmaskBits #MA_BACKVISIBLE, now
; > endif
;
;   See Also:
;   <staPPUmaskBits>

.macro ldaPPUmaskBits mask
    
    lda ppu::PPUmask
    .ifnblank mask
        and mask
    .endif
    
.endmacro

; --------------------------------------------------------------------------------------------
; Function: staPPUmaskBits
;
; Parameters:
;
;   setbits - Optional - Bits to set.
;   update - Optional - any value.
;
;   This macro will store the current value of register A to <ppu::PPUmask>.
;   If setbits is sent, it will OR setbits with register A before storing to <ppu::PPUmask>.
;   If update is sent (non-blank), register A will also be stored immediately to the PPU MASK port.
;
;   Example: Enable background rendering on the PPU
; > ldaPPUmaskBits
; > staPPUmaskBits #MA_BACKVISIBLE, now ; save to shadow register, and the PPU.
;
;   See Also:
;   <ldaPPUmaskBits>

.macro staPPUmaskBits setbits,update
    
    .ifnblank setbits
        ora setbits
    .endif
    
    sta ppu::PPUmask
    
    .ifnblank update
        sta PPU_MASK
    .endif
.endmacro

; --------------------------------------------------------------------------------------------
; Function: updatePPUnametable
;
; Parameters:
;
;   none
;
;   Use this after VRAM update to reset to proper nametable for rendering.
;   It will set the lowest two bits in <ppu::PPUctrl> and PPU CTRL to the value in <ppu::PPUnametable> immediately.

.macro updatePPUnametable

    ldaPPUctrlBits #%11111100    ; get all bits but nametable bits
    ora ppu::PPUnametable
    staPPUctrlBits , now
    
.endmacro

; --------------------------------------------------------------------------------------------

.macro updateScroll
    
    lda ppu::PPUxScroll
    sta PPU_SCROLL
    lda ppu::PPUyScroll
    sta PPU_SCROLL
    
.endmacro

; --------------------------------------------------------------------------------------------
; Function: setPPUnametable
;
; Parameters:
;
;   value - value of PPU nametable to use
;
;   Stores the value to <ppu::PPUnametable> 

.macro setPPUnametable value
    
    lda value
    sta ppu::PPUnametable
    
.endmacro

; --------------------------------------------------------------------------------------------
; Function: PPU_RENDER_OFF
;
; Mask sprite and background enable bits of <ppu::PPUmask>
;
; Does not update the PPU port -- wait for NMI to do this.

.macro PPU_RENDER_OFF
    lda ppu::PPUmask
    and # <~(MA_SPRITESVISIBLE|MA_BACKVISIBLE)
    sta ppu::PPUmask
.endmacro

; --------------------------------------------------------------------------------------------
; Function: PPU_RENDER_ON
;
; Enables sprite and background enable bits of <ppu::PPUmask>
;
; Does not update the PPU port -- wait for NMI to do this.
    
.macro PPU_RENDER_ON
    lda ppu::PPUmask
    ora #(MA_SPRITESVISIBLE|MA_BACKVISIBLE)
    sta ppu::PPUmask
.endmacro

; --------------------------------------------------------------------------------------------
; Function: updatePPUctrl
;
; For use during NMI.
;
; Loads the value of <ppu::PPUctrl> and sends it to PPU CTRL.

.macro updatePPUctrl   
 
    lda ppu::PPUctrl
    sta PPU_CTRL
    
.endmacro

; --------------------------------------------------------------------------------------------
; Function: updatePPUmask
;
; For use during NMI.
;
; Loads the value of <ppu::PPUmask> and sends it to PPU MASK.

.macro updatePPUmask
    
    lda ppu::PPUmask
    sta PPU_MASK
    
.endmacro


; --------------------------------------------------------------------------------------------
; Function: setPPUaddress
;
; For use during NMI, or with rendering off.
;
; Sets the address for PPU vram.

.macro setPPUaddress newaddress

    .if  .xmatch(.mid(0,1, {newaddress}), #) ; immediate mode
        lda  #> .right(.tcount( { newaddress } ) - 1, { newaddress })
        sta PPU_ADDRESS
        lda  #< .right(.tcount( { newaddress } ) - 1, { newaddress })
        sta PPU_ADDRESS
    .else
        lda newaddress+1
        sta PPU_ADDRESS
        lda newaddress
        sta PPU_ADDRESS
    .endif

.endmacro

; --------------------------------------------------------------------------------------------

.macro setPPUxScroll value
    
    lda value
    sta ppu::PPUxScroll
    
.endmacro

; --------------------------------------------------------------------------------------------

.macro setPPUyScroll value
    
    lda value
    sta ppu::PPUyScroll
    
.endmacro

; --------------------------------------------------------------------------------------------

.macro updatePPUscroll
    
    bit PPU_STATUS
    lda ppu::PPUxScroll
    sta PPU_SCROLL
    lda ppu::PPUyScroll
    sta PPU_SCROLL
    
.endmacro


.macro loadBGpalEntry reg, palette, entry

    .ifnblank reg
        .if .xmatch({reg}, {a})
            lda ppu::paletteShadow + (palette * 4) + entry
        .elseif .xmatch({reg}, {x})   
            ldx ppu::paletteShadow + (palette * 4) + entry 
        .elseif .xmatch({reg}, {y})   
            ldy ppu::paletteShadow + (palette * 4) + entry
        .else
            .error "Unknown register value."
        .endif
    .else
        lda ppu::paletteShadow + (palette * 4) + entry
    .endif

.endmacro

.macro storeBGpalEntry reg, palette, entry

    .ifnblank reg
        .if .xmatch({reg}, {a})
            sta ppu::paletteShadow + (palette * 4) + entry 
        .elseif .xmatch({reg}, {x})   
            stx ppu::paletteShadow + (palette * 4) + entry 
        .elseif .xmatch({reg}, {y})   
            sty ppu::paletteShadow + (palette * 4) + entry 
        .else
            .error "Unknown register value."
        .endif
    .else
        sta ppu::paletteShadow + (palette * 4) + entry 
    .endif

.endmacro

.macro ldaSpritePalEntry palette, entry

    .ifnblank reg
        .if .xmatch({reg}, {a})
            lda ppu::paletteShadow + (palette * 4) + entry + $10
        .elseif .xmatch({reg}, {x})   
            ldx ppu::paletteShadow + (palette * 4) + entry + $10
        .elseif .xmatch({reg}, {y})   
            ldy ppu::paletteShadow + (palette * 4) + entry + $10
        .else
            .error "Unknown register value."
        .endif
    .else
        lda ppu::paletteShadow + (palette * 4) + entry + $10
    .endif

.endmacro

.macro storeSpritePalEntry reg, palette, entry
    
    .ifnblank reg
        .if .xmatch({reg}, {a})
            sta ppu::paletteShadow + (palette * 4) + entry + $10
        .elseif .xmatch({reg}, {x})   
            stx ppu::paletteShadow + (palette * 4) + entry + $10
        .elseif .xmatch({reg}, {y})   
            sty ppu::paletteShadow + (palette * 4) + entry + $10
        .else
            .error "Unknown register value."
        .endif
    .else
        sta ppu::paletteShadow + (palette * 4) + entry + $10
    .endif

.endmacro

.define PPUxy (_X, _Y, c ) ($2000 + $20 * _Y + _X) c

.macro calcNametableAddr nameX, nameY, NT

    .ifblank NT
       nametable .set 0
    .else
        nametable .set NT
    .endif

    nameTableAddress .set ($2000 | ($0400 * nametable)) + $20 * _Y + _X)
    
.endmacro

.macro calcAttribAddr nameX, nameY, NT
    
    .local attrTable, nametable
    
    .ifblank NT
       nametable .set 0
    .else
        nametable .set NT
    .endif
    
    attrTableOffset     .set   ( nameY / 4) * 8 | (nameX / 4)     
    attrTableAddress    .set   $23C0 | ( nametable << 10 )  | ( nameY / 4) * 8 | (nameX / 4)
    attrTableBits       .set    ( ( (nameX / 2) .mod 2 ) * 2 ) | ( ( (nameY / 2) .mod 2 ) * 4 )
    
.endmacro


.endif

