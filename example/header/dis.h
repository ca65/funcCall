; --------------------------------------------------------------------------------------------
; File: dis.h
;
; Interface for dis.inc
; Disassembly of one instruction to a given string

.ifndef _DIS_H_
_DIS_H_= 1

.include "header/funcCall.h"

.scope dis
    declarefunc setAddress, { addr ax }             ; These declarations must match those defined in the implementation
    declarefunc disassemble, { stringPtr ax }, 1
.endscope

.endif
