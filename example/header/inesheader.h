
__NUM_16K_PRG_BANKS__  = 2
__NUM_8K_CHR_BANKS__   = 1
__MAPPER__             = 0 ; 0 = NROM, 4 = MMC3
__MIRRORING__          = 1 ; Horizontal

.pushseg
.segment "INESHDR"
    .byte $4E,$45,$53,$1A                           ;  magic signature
    .byte __NUM_16K_PRG_BANKS__
    .byte __NUM_8K_CHR_BANKS__
    .byte ( __MAPPER__ & $F ) << 4 | __MIRRORING__
    .byte __MAPPER__ & $F0
.popseg
