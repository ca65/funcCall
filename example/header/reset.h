; library imports

; Section: Scoped Interface:

; Class: reset
.ifndef _H_RESET_
_H_RESET_ = 1

.include "header/funcCall.h"

.scope reset

    declarefunc resetRoutine
    
.endscope

.macro setEntryFromReset
    .export ___entryFromReset
    ___entryFromReset:
.endmacro

.endif
