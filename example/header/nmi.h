; --------------------------------------------------------------------------------------------
; File: nmi.h

; interface for NMI.inc

.ifndef _NMI_H_
_NMI_H_ = 1

.include "header/funcCall.h"

.scope nmi
    ; functions
    declarefunc NMIRoutine
    declarefunc NMIentry
    declarefunc waitForNMI
   ; declarefunc doOAMUpdateNMI
   ; declarefunc yield
    declarefunc addCallback, { start x, funcPtr ay }
    declarefunc removeCallback, { start x }
    declarefunc waitForXFrames, { frames x }
    
    ; variables

    .importZP frameReady  
    .importZP frameCounter
    .importZP NMIdisabled

    
.endscope





.endif


