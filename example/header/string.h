; --------------------------------------------------------------------------------------------
; File: string.h
;
; String related macros and routines.
;

.ifndef _STRING_H_
_STRING_H_ = 1

.include "header/ppu.h"
.include "header/funcCall.h"

.scope str
        declarefunc print, { _x .byte, _y  a, string xy }, 1
        declarefunc byteToHex, { value a }
.endscope

; --------------------------------------------------------------------------------------------
; jPrint and jPrintSetCursor (j -> justify )
;
; macros to help arranging and justifying text on the screen
; they result in defining data suitable for the generic 
; transferVramBuffer routine, and outputting a call to load the vram buffer.
; only support for 1st nametable as is

; define some ca65 identifiers to save values between macro calls:

.scope jusprint
    strstart    .set 0
    strpos      .set 0
    cursorx     .set 0
    cursory     .set 0
    col         .set 30 ; default
    max_col     .set 0
.endscope

; macro to help jPrint to figure out what part of the string to extract:

.macro ___justifyText string
        ; not at end of string yet, and no space?
        .if ( jusprint::strpos < .strlen(string) - 1 ) &&  ( .strat(string, jusprint::strpos) <> ' ' )
            ; keep iterating over the string
            jusprint::strpos .set jusprint::strpos + 1 
            jusprint::col .set jusprint::col + 1
            ___justifyText string  
            
        ; over the max? adjust and exit macro (even if at end of string)
        .elseif jusprint::col >= jusprint::max_col
            ; use ( jusprint::strpos - jusprint::oldStrPos ) to get the old col. rather than another ident
            jusprint::col .set jusprint::col - ( jusprint::strpos - jusprint::oldStrPos ) - 1 ; -1 remove trailing space
            jusprint::strpos .set jusprint::oldStrPos + 1                                     ; +1 skip space

        ; if not end of string, then under the max, so save where we are and try the next word
        .elseif jusprint::strpos <> .strlen(string) - 1
            jusprint::oldStrPos .set jusprint::strpos     ; save old position in case the next word is too long
            jusprint::strpos .set jusprint::strpos + 1    ; skip over this space
            jusprint::col .set jusprint::col + 1          ; increase the column as well
            ___justifyText string                         ; repeat
        .endif
.endmacro

; -----
; set the max column allowed for text to be displayed

.macro jPrintSetMaxCol column
    jusprint::max_col .set column
.endmacro

; -----
; set the initial PPU x,y for where to start displaying text. 

.macro jPrintSetCursor _x, _y

    .if .not .blank(_x)
        jusprint::cursorx .set _x
    .endif
    .if .not .blank(_y)
        jusprint::cursory .set _y
    .endif

.endmacro

; -----

.macro jPrint string

    .local nt_address, stringdata
    
    .if .blank(string)
        ; in this case, only result will be incrementing cursory
        jusprint::cursory .set jusprint::cursory + 1
        .exitmacro
    .endif
    
    jusprint::col .set jusprint::cursorx
    
    jusprint::oldStrPos .set jusprint::strpos ; do we need this?
    ___justifyText string
    
    .pushseg
    .segment "RODATA"
    
    nt_address .set $2000 + $20 * jusprint::cursory + jusprint::cursorx
    
    stringdata: .byte >nt_address, <nt_address, jusprint::col - jusprint::cursorx
    
    .repeat jusprint::col - jusprint::cursorx + 1, i
        .byte .strat( string, i + jusprint::strstart )
    .endrepeat
    
    .popseg
    
    call ppu::loadVRAMBuf, { &stringdata }

    jusprint::cursory .set jusprint::cursory + 1
    
    ; not at end of string? process next part of string
    .if jusprint::strpos <> .strlen(string) - 1
        jusprint::col .set jusprint::cursorx
        jusprint::strstart .set jusprint::strpos
        jPrint string
    .else
        jusprint::strstart     .set 0
        jusprint::strpos       .set 0
    .endif
    
.endmacro

; --------------------------------------------------------------------------------------------

.macro ppuString _X, _Y, _string, NT

    .local nametable

    .pushseg
    .segment "RODATA"
    
        .if .blank (NT)
            nametable .set 0
        .else
            nametable .set NT
        .endif
   
    .byte >($2000 | $0400 * nametable + $20 * _Y + _X), <($2000 | $0400 * nametable + $20 * _Y + _X), <(.strlen(_string) - 1), _string

    .popseg
.endmacro

; --------------------------------------------------------------------------------------------
; .macro evalString string, _strlen
; Helper macro for .macro puts
; - Evaluates a string, counting the length, while replacing any hex bytes inside opening and closing '`'
; - Builds an array of identifiers that holds the byte values of the string. 
;

.macro evalString string, _strlen
    
    .local char
    .local hex
    .local expected
    .local hiNibNext
    .local nib
    .local strlen
    .local hiNib
    expected .set 0         ; 1 for hex nib
    hiNibNext .set 0
    strlen .set 0
    
    .repeat .strlen(string), i ; 0 to length
        char .set .strat( string , i )
        .if char = '`' || expected = 1
            .if char = '`' 
                .if expected = 1 ; we are done  
                    expected .set 0
                    .if hiNibNext
                        .error "Uneven hex digits."
                    .endif
                .else
                    expected .set 1
                .endif
            .else
                .if char >= 'A' && char <= 'F'
                    nib .set char - 55
                .elseif char >= 'a' && char <= 'z'
                    nib .set char - 87
                .elseif char >= '0' && char <= '9'
                    nib .set char - 48   
                .else
                    .error "Hex string expected."
                .endif
                
                .if hiNibNext
                    .ident(.sprintf("___byteList_%04X", strlen ) ) .set hiNib << 4 | nib
                    strlen .set strlen + 1
                    hiNibNext .set 0
                .else
                    hiNib .set nib
                    hiNibNext .set 1
                .endif
            .endif
        .else
            .ident(.sprintf("___byteList_%04X", strlen )) .set char
            strlen .set strlen + 1
        .endif
    .endrepeat
    .if hiNibNext
        .error "Uneven hex digits."
    .endif
    _strlen .set strlen
.endmacro

; --------------------------------------------------------------------------------------------
; define and print a constant ROM string, with possible hex data
; somewhat obsoleted by jPrint?
;
.macro puts _X, _Y, _string, NT
    
        .local nametable
        .local strlen
        .local stringData
      
        .if .blank (NT)
            nametable .set 0
        .else
            nametable .set NT
        .endif
        
        evalString _string, strlen
        
        .pushseg
        .segment "RODATA"
    
            stringData:
            
            .byte >(($2000 | ($0400 * nametable)) + $20 * _Y + _X)
            .byte <(($2000 | ($0400 * nametable)) + $20 * _Y + _X)
            .byte strlen - 1
            .repeat strlen, i
                .byte .ident(.sprintf("___byteList_%04X", i ))
            .endrepeat
        
        .popseg
        
        call ppu::loadVRAMBuf, { &stringData }

.endmacro

; --------------------------------------------------------------------------------------------
.endif
