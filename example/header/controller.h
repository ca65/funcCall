; --------------------------------------------------------------------------------------------
; File: controller.h
; Interface for controller.inc

.ifndef _CONTROLLER_H_
_CONTROLLER_H_ = 1

.include "header/funcCall.h"

.scope controller
    
    declarefunc readPort, { player x }
    
    ; variables
    .importZP pressed
    .importZP pressedLastFrame  
    .importZP pressedNew    
    .importZP releasedNew
    
.endscope

.endif

