; --------------------------------------------------------------------------------------------
; File: funcCall.h
;
; Section: Macros for Creating and Calling Functions:
;
; Summary:
;
; These macros allow declaration, definition and calling of functions with parameters.
; Functions use a user defined space in zeropage for both parameters and local variables.
;
; Declarations:
;
; Declaring functions is not required. Forward declarations allow functions to be imported, or calls to be made before the
; full definition. Functions can be declared by invoking the <declarefunc> macro with the first parameter being the 
; new function name, the second parameter are the function parameters enclosed in curly brackets.  
; The macro will use *.global* to allow for the import the function or definition later in the same source file.
;
; Definition:
;
; Functions can be defined by invoking the <func> macro with the first parameter being the function name, 
; and the second, the parameters enclosed in curly brackets. The parameters must match the parameters used before if
; the function was first named with <declarefunc>. It will check for matching names, and memory sizes of parameters.
;
; Calling:
;
; The call will verify the parameters and attempt to optimize the order of parameter loading.
; Functions can be called by invoking the <call> macro with the first parameter being the function name, 
; and the parameters following enclosed in curly braces. Parameters in the call can be in any order when 
; using the identifiers declared in the definition. Parameters can be omitted (they will be undefined).
; Calls can also be made without using the names of the parameters. In this case, they must be in the same 
; order as the declaration.
;
; Parameters:
;
; Parameters can be defined as a valid ca65 identifier, followed by a size or register designation.
; Valid parameter sizes/types:
;
; ---code
; .byte, .word, .addr, .dword, a, x, y, ax, ay, xy
; ---
;
; Local variables:
;
; Locals can be defined inside a function body. They use the same memory area as the parameter memory.
; They can be followed by a constant to define multiple.
;
; Valid locals sizes:
;
; ---code
; .byte, .word, .addr, .dword
; ---
;
; Declaring a function:
;
; This example will create a parameter list with two byte size parameters.
; The assembler will save all information needed as identifiers.
;
; Examples:
;
; Define a function:
; ---code
; func add, { one .byte, two .byte }
; 
; locals
;     sum .word
; endlocals
; 
;     lda param one
;     clc
;     adc param two
;     ldx #$00
;     bcc :+
;     inx
;     :
;     rts
; 
; endfunc
; ---
;
; Calling a function:
;
; ---code
; call addTwo, { one: #$10, two: foo } 
; ---
;
; Or, the same result without named parameters:
;
; ---code
; call addTwo, { #$10, foo } 
; ---
;
; The macro will load all parameters as requested before using a JSR to call the function.
; Note: Registers names can be referenced directly as well, the defined parameter names for registers are not required.
; Registers can be used for source values or parameters.
;
; ---code
; call myFunc, { foo: #$10, x: <BAZ, a : a }  
; ---
;
; In the last example, no code will be output regarding register a, but it should still be included to indicate
; that it is a needed parameter to avoid register a being used to load the other parameters.
;
; Operators:
;
; When calling, you can use normal ca65 syntax including the following operators:
;
; Immediate value: #
;
; Lowbyte: <
;
; Highbyte: >
;
; also:
;
; Address: & 
;
; The address operator will force the value to be interpreted as an immediate word size value, implying an address is being passed.
;
; Note: If there is a need, values can be prefixed with one of these operators to force a size for the value. 
; If there is a problem with the size of the value, a size can also be forced with a cast. 
;
; Example:
;
; ---code
; call myFunc, { foo: (.byte)baz, bar: (.word)fum + 3 }  
; ---
;
; Locals:
;
; Locals can be defined inside a function body. They use the same memory as the parameter memory.
;
; Example:
;
; ---code
; func copyMem, { source ax, dest .addr, bytes y }
; 
;     locals 
;         source  .addr
;     endlocals
;     
;     sta local source
;     stx local source + 1
;     
;     :
;         lda (local source),y
;         sta (param dest), y
;     dey
;     bne :-
;     
;     rts
;     
; endfunc
; ---
; 
; Note: use <param> before a parameter name to access parameters.
; Use <local> before a local name.
;
; Use <fparam> to access function parameters after a call:
;
; ---code
; lda fparam MyFunc::sum 
; lda {fparam MyFunc::sum}+1 
; ---
;
; Nested Calls:
;
; If a function that has parameters and/or locals is going to call a function, use the following syntax:
; For a function that is going to be called from within a function:
; 
; ---code
; declarefunc myfunc, { f .byte }, 7
; ---
;
; Any functions that will call myfunc can use up to 7 bytes of memory without conflict.
; This is not needed if it is clear there are no conflicts.
;
; Known limitations:
;
; - A call to a function that loads registers explicitly will not check if the declaration declared the use of registers, 
;   meaning you can always explicitly load registers for the call.
; - When calling a function using parameter names, parameters can be loaded more than once. The call will work, just wasteful.
; - Trying to load x with y or y with x will fail due to the limitation of the 6502: These macros don't use any additional temporary memory.
; - Loading registers out of order may fail with en error. Register sources are loaded first. Example failed parameter list: { a: x, y: a } 
; - The high byte(s) of parameters loaded with a smaller source will be cleared to zero, except when using a CPU register as a source - only one byte will be stored. 
; - Sign extending not supported.
; - The calling macro tries t verify the passed parameter isn't bigger than the declared parameter. It is sometimes difficult with ca65 to 
;   determine the size of an identifier without providing the full scope, so the macro will default to the destination size 
;   for parameters when the identifier is unknown during assembly.
;
; --------------------------------------------------------------------------------------------

; Additional Notes:
;
; Macro names in the file starting with three '_' are intended to be hidden from the user. They are macros to help with the main macros.
; An identifier is created for each function: "functionname___declared"
; A scope is created for each function defined as "functionname___params". Identifiers for parameters are created inside this scope as well as:
; 
; parameterMemoryStart ; the address in memory where parameters/locals start.
; parameterMemorySize  ; the number of bytes used for parameters.
; localMemorySize      ; the number of bytes used for locals (follows parameters, except in library mode, parameters shrink down from a higher address, followed by locals.)
; defined              ; if the function body has been defined.
; 
; --------------------------------------------------------------------------------------------
 

.ifndef _FUNC_CALL_
_FUNC_CALL_ = 1

.include "header/ndxdebug.h"
;--------------------------------------------------
; Where in memory parameters and local variables will stored - Set these two values.
; Don't (or be careful) using these memory locations.

; Constant: _PARAMETER_MEMORY_START_
; Where in zeropage to start storing parameters.
; > _PARAMETER_MEMORY_START_ 0

.ifndef ::_PARAMETER_MEMORY_START_
    ::_PARAMETER_MEMORY_START_ = 0
.endif
    
; How much zeropage space parameters can use

; Constant: _PARAMETER_MEMORY_MAXSIZE_
; How many bytes to use for parameters.
; > _PARAMETER_MEMORY_MAXSIZE_ 16

.ifndef ::_PARAMETER_MEMORY_MAXSIZE_
    ::_PARAMETER_MEMORY_MAXSIZE_ = 16
.endif

; Constant: _CALL_WARNINGS_
; Output a ca65 warning message for any nested calls, so they can be checked for parameter memory conflicts.
; Turn on warnings:
; > _CALL_WARNINGS_ 1

.ifndef ::_CALL_WARNINGS_
    ::_CALL_WARNINGS_ = 0
.endif

.assert ::_PARAMETER_MEMORY_START_ + ::_PARAMETER_MEMORY_MAXSIZE_ <= $100, error, "Function memory should be in the zeropage."


;--------------------------------------------------
; Group: Macros

; Function: fparam
;
; Parameters:
;
; p - FunctionName::ParameterName
;
; Inline macro to help access parameters after a call.
;
; Example:
; 
; ---code
; call foo, { bar: foobar }
;
; lda fparam foo::bar
; --- 
;
.define fparam(p) .left(.tcount(p) - 4, p)::.ident(.sprintf("%s___params", .string( .left(1, .right(3, p )) )))::.right(1,p)

; Function: flocal
;
; Parameters:
;
; p - FunctionName::localname
;
; Inline macro to help access locals after a call.
; This macro won't work when a function has been imported from another module.
;

.define flocal(p) .ident(.string( .left(.tcount({p}) - 2 ,{p} ) ))::.ident(.concat(.string(.left(1, {.right(3, {p})} )) , "___locals"))::.ident(.string(.right(1, {p} )))

; --------------------------------------------------------------------------------------------
; Some private temp values for this file:

.scope ___functionMacro
    
    regAaccessedFlag    .set 0  ; keep track of register usage for loading parameters or processing declarations.
    regXaccessedFlag    .set 0     
    regYaccessedFlag    .set 0
    parameterAddress    .set 0  ; keep track of parameter addresses when processing parameters
    functionBodyActive  .set 0  ; true (1) if we are in a function body
    global              .set 1  ; when 0 - don't declare the function as global: when defining a function without declaration.
    libraryMode         .set 0  ; if this flag is on, all parameters will be moved as far as possible to the end of parameter memory.
    paramSizeTotal      .set 0  ; temporary to count the amount of memory needed for parameters
    previousImm         .set 0  ; save the previous immediate value when moving multiple immediate value bytes
    parameterCounter    .set 0  ; count the parameters during declaration or calling (for calling without named parameters).
    
.endscope

; --------------------------------------------------------------------------------------------
; array for saving parameter sizes/types

.define ___paramListSizes(i) .ident(.sprintf("param_%02X", i))

; --------------------------------------------------------------------------------------------
; Local macro to match the next token in the token list passed
; it won't find the token in the very first (0) position!
.if .not .definedmacro( ___findToken )
.macro ___findToken param, tok, position
    .repeat .tcount( {param} ), c
        .if .xmatch( {.mid(c, 1, {param}) }, {tok})
            .if .not position ; do not remove this check
                position .set c
                .exitmacro
            .endif
        .endif
    .endrepeat
.endmacro
.endif
; --------------------------------------------------------------------------------------------
; Macro for copying parameters.
; This will output the appropriate amount of load/store instructions
; Arguments: CPU register to use, source, destination, dest. size, source size, and immediate mode flag
; Source must be the same size or smaller then the dest.
; If the source is smaller, load a zero, skip further loads, and fill the destination with zero
; We could optionally sign extend, not implemented yet.
; Arguments don't need error checking, should be valid from calling macro

; small macro for ___moveMem macro, to skip loading the same immediate value if possible

.macro ___LOAD_imm value

    .if .const(value)
        .if .not ( value = ___functionMacro::previousImm )
            LOAD # ( value )
            ___functionMacro::previousImm .set value 
        .endif
    .else
        LOAD # ( value )
    .endif
    
.endmacro

.macro ___moveMem reg, source, dest, destsize, sourcesize, imm

    .local sourcecount
    sourcecount .set sourcesize

    .if .xmatch(reg, a)
        .define LOAD  lda
        .define STORE sta
    .elseif .xmatch(reg, x) 
        .define LOAD  ldx
        .define STORE stx
    .else
        .define LOAD  ldy
        .define STORE sty
    .endif
    
    ; set to a value that couldn't fit into one byte to avoid matching the first byte
    ___functionMacro::previousImm .set $0100
    
    .if imm
        .repeat destsize, i
                ; nothing to do for source if sourcecount is -1
                .if sourcecount <> -1 
                    .if sourcecount = 0
                        LOAD #0
                    .else
                        ___LOAD_imm <( ( source ) >> (8 * i) )
                    .endif
                    sourcecount .set sourcecount - 1
                .endif
                STORE i+dest
        .endrepeat
    .else
        .repeat destsize, i
                ; nothing to do for source if sourcecount is -1
                .if sourcecount <> -1 
                    .if sourcecount = 0
                        LOAD #0
                    .else   
                        LOAD i + source
                    .endif
                    sourcecount .set sourcecount - 1
                .endif
                STORE i+dest
        .endrepeat
    .endif
    
    .undefine LOAD
    .undefine STORE
    
.endmacro

; --------------------------------------------------------------------------------------------
; Sub macro to load parameters before calling the function
; check each parameter, on each pass: 1) move regs to memory 2) move regs to regs 3) move mem to mem 4) move memory to regs
; marks if regs were already used as a destination in step 2)
; Requires these values to be set before calling:
; ___functionMacro::parameterAddress ; set to the function's first used memory address
; ___functionMacro::parameterCounter ; set to 0

.macro ___loadParams param, pass, parameterMode

    .if .blank({param})
        ; nothing to do
        .exitmacro
    .endif

    .local comma_pos
    .local colon_pos
    .local parameterDestSize  
    .local parameterSourceSize 
    .local immMode
    .local castbase
    .local destinationAddress
    comma_pos .set 0            ; position of comma in full parameter list
    colon_pos .set 0            ; position of colon, should always be 1
    parameterDestSize   .set 0  ; size of memory for parameter to be loaded
    parameterSourceSize .set 0  ; size of source data
    immMode .set 0              ; if copy should be immediate mode
    destIsReg .set 0            ; if the destination matched a register
    sourceIsReg .set 0          ; if the source matched a register
    destinationAddress  .set 0  ; if loading unlisted parameters
    
    ; --------------------------------------------------------------------------------------------
    ; find comma in param list. It's okay to not find one if there is only one parameter
    ___findToken {param}, {,}, comma_pos
    ; define and then work with one parameter at a time
    .if comma_pos
        .define _PARAM_ .mid(0, comma_pos, {param})
    .else
        .define _PARAM_() param  ; brackets prevent issues with 'param' opening '('
    .endif
    
    ; ___findToken won't find a leading comma, check if there is a leading comma, and set comma_pos it to -1 to indicate this.
    .if .xmatch( { .left( 1, {_PARAM_} ) } , {,} )
        comma_pos .set -1
    .endif
    ; --------------------------------------------------------------------------------------------

    ; parameterMode non zero means there is there a colon in the parameter list: named parameter loading
    .if .not ( parameterMode = 0 )
        ; called with named parameters, find parameter name, left of colon
        ; split parameter into destination and source at the colon
    
        ; fix for loading register a:
        ; ca65 will match 'a:' with no space to a single token, so check for it
        .if .xmatch( .left(1, { _PARAM_ }) , a:)
            .define _T_DEST_ a
            .define _T_SOURCE_ .right( .tcount({_PARAM_}) - 1, {_PARAM_})
        .else
            ; get the source and dest. which is separated by a ':' should always be position 1
            ___findToken {param}, {:}, colon_pos
            .if colon_pos <> 1
                .error "Bad parameter, invalid ':'"
                .fatal "STOP"
            .endif
            .define _T_DEST_  .mid(0, colon_pos, {_PARAM_})
            .define _T_SOURCE_ .mid(colon_pos + 1,  .tcount ( { _PARAM_ } ) - colon_pos - 1, {_PARAM_}) 
        .endif    
    .else
    
    ; --------------------------------------------------------------------------------------------
        ; no names for parameters
        ; figure out the destination address and size without named parameters:
        ; count parameters in the order they are declared/called
        ; in this case source is just the parameter passed:
        .define _T_SOURCE_() _PARAM_
        
        ; too many parameters passed?
        .if .not .defined(_FUNC { ___params } ::___paramListSizes { ___functionMacro::parameterCounter })
            .error "Too many parameters."
            .fatal "STOP"
        .endif
        
        parameterDestSize .set _FUNC { ___params } ::___paramListSizes { ___functionMacro::parameterCounter }
        ; if parameter destination is a register, mark temp destination (_T_DEST) as appropriate register, let the register code in the next block process it
        ; parameterDestSize will get fixed up later (will be 1 or 2 bytes depending on 1 or 2 registers).
        .if parameterDestSize > $FF
            .if parameterDestSize     =       ( 1 << 8 )        ; a
                .define _T_DEST_ a
            .elseif parameterDestSize =     ( ( 1 << 8 ) | 1 )  ; x
                .define _T_DEST_ x
            .elseif parameterDestSize =     ( ( 1 << 8 ) | 2 )  ; y
                .define _T_DEST_ y
            .elseif parameterDestSize =     ( ( 1 << 8 ) | 3 )  ; ax
                .define _T_DEST_ ax
            .elseif parameterDestSize =     ( ( 1 << 8 ) | 4 )  ; ay
                .define _T_DEST_ ay
            .elseif parameterDestSize =     ( ( 1 << 8 ) | 5 )  ; xy
                .define _T_DEST_ xy
            .endif
        .else
            ; not a register, so is a zp memory location:
            .define _T_DEST_ ___functionMacro::parameterAddress
            .define _DEST_ _T_DEST_
            .define _DEST_STR_ .sprintf("(parameter %d)", ___functionMacro::parameterCounter + 1)
        .endif
    .endif
    
    ; --------------------------------------------------------------------------------------------
    ; In either parameter mode: check if destination is explicitly a CPU register. 
    ; Named parameter mode, user would have explicitly named a register, in value only mode, register assigned in block above.
    
    .if .xmatch( { _T_DEST_ } , { a } ) || .xmatch( { _T_DEST_ } , { y } ) || .xmatch( { _T_DEST_} , { x } ) 
        .define _DEST_ _T_DEST_
        destIsReg .set 1
        parameterDestSize .set 1
        ; for nicer error messages:
        .if .xmatch(_T_DEST_,a) 
            .define _DEST_STR_ "register a"
        .elseif .xmatch(_T_DEST_,x) 
            .define _DEST_STR_ "register x"
        .elseif .xmatch(_T_DEST_,y) 
            .define _DEST_STR_ "register y"
        .endif
        
    ; Check if destination is explicitly a CPU register pair:
    .elseif .xmatch( { _T_DEST_ }, { ax } ) || .xmatch( { _T_DEST_ }, { ay } ) || .xmatch( { _T_DEST_ }, { xy } )     
        .define _DEST_ _T_DEST_
        destIsReg .set 1
        parameterDestSize .set 2
        ; for nicer error messages:
        .if .xmatch(_T_DEST_,ax) 
            .define _DEST_STR_ "register ax"
        .elseif .xmatch(_T_DEST_,ay) 
            .define _DEST_STR_ "register ay"
        .elseif .xmatch(_T_DEST_,xy) 
            .define _DEST_STR_ "register xy"
        .endif
       
    ; If in named parameter mode, check if destination matches a declared parameter (parameters are declared in a scope named for the function)
    ; If it matches a declared parameter, set it in the define. Also set the size of the destination
    .elseif .not ( parameterMode = 0 )
        .if .defined( _FUNC { ___params } :: .ident( .string(_T_DEST_)  ) )
            ; find if the destination is marked as a register, if not..
            ; find the size of the destination
            .if ( _FUNC { ___params } :: .ident( .string(_T_DEST_)  ) ) =  ( 1 << 8 ) ; a
                .define _DEST_ a
                parameterDestSize .set 1
                destIsReg .set 1
                .define _DEST_STR_ .concat(.string(_T_DEST_), " (register a)" ) ; these defines are for nicer error messages.
            .elseif ( _FUNC { ___params } :: .ident( .string(_T_DEST_) ) ) = ( ( 1 << 8 ) | 1 ) ; x
                .define _DEST_ x
                parameterDestSize .set 1
                destIsReg .set 1
                .define _DEST_STR_ .concat(.string(_T_DEST_), " (register x)" )
            .elseif ( _FUNC { ___params } :: .ident( .string(_T_DEST_) ) ) = ( ( 1 << 8 ) | 2 ) ; y
                .define _DEST_ y
                parameterDestSize .set 1
                destIsReg .set 1
                .define _DEST_STR_ .concat(.string(_T_DEST_), " (register y)" )
            .elseif ( _FUNC { ___params } :: .ident( .string(_T_DEST_) ) ) = ( ( 1 << 8 ) | 3 ) ; AX
                .define _DEST_ ax
                parameterDestSize .set 2
                destIsReg .set 1
                .define _DEST_STR_ .concat(.string(_T_DEST_), " (register ax)" )
            .elseif ( _FUNC { ___params } :: .ident( .string(_T_DEST_) ) ) = ( ( 1 << 8 ) | 4 ) ; AY
                .define _DEST_ ay
                parameterDestSize .set 2
                destIsReg .set 1
                .define _DEST_STR_ .concat(.string(_T_DEST_), " (register ay)" )
            .elseif ( _FUNC { ___params } :: .ident( .string(_T_DEST_) ) ) = ( ( 1 << 8 ) | 5 ) ; XY
                .define _DEST_ xy
                parameterDestSize .set 2
                destIsReg .set 1
                .define _DEST_STR_ .concat(.string(_T_DEST_), " (register xy)" )
            .else
                .define _DEST_ _FUNC { ___params } ::.ident( .string(_T_DEST_))
                .define _DEST_STR_ .string(_T_DEST_)
                parameterDestSize .set .sizeof(  _FUNC { ___params } :: .ident( .string(_T_DEST_)  ) )
            .endif
        .else
            .error .sprintf("Parameter name not found: %s", .string(_T_DEST_) )
            .fatal "STOP"
        .endif
    .endif
    
    ; --------------------------------------------------------------------------------------------
    ; Destination is determined:    
    ; Before we can continue, check if our source is empty. This is indicated by a comma_pos of -1
    ; This is okay (but should only happen) when parameterMode = 0
    ; if parameter is empty, skip most of the rest of the routine.
    .if comma_pos <> - 1
        
        ; define _SOURCE_ and find size
        ; 1) check for special '&' operator for address - set immediate flag and set size to two (assume 16 bit address), remove '&' from tokens
        ; 2) check if immediate - set flag and remove '#' from tokens, look for numeric constant and set size
        ; 3) check if explicitly cast to a size, with (.byte | .word | .addr | .dword) plus optional size multiplier
        ; 3) check if using a CPU register - set size to byte
        ; 4) try to find the size of a symbol
        
        ; Check if source is using '&' as an address operator (immediate address value):
        .if .xmatch( {.left(1, {_T_SOURCE_} ) } , {&} ) 
            parameterSourceSize .set 2
            immMode .set 1
            ; remove the '&'
            .define _SOURCE_ .mid(1, .tcount( {_T_SOURCE_} ) - 1 , {_T_SOURCE_})
            
        ; Check if source is an immediate value:
        .elseif .xmatch( .left(1,{_T_SOURCE_}), # ) ; if immediate
            immMode .set 1
            ; remove the '#'
            .define _SOURCE_ .mid(1, .tcount( {_T_SOURCE_} ) - 1 , {_T_SOURCE_})
            ; check if there is a constant
            .if .const( _SOURCE_ ) 
                .if ( _SOURCE_ & $FFFF0000 ) 
                    parameterSourceSize .set 4
                .elseif ( _SOURCE_ & $FF00 )
                    parameterSourceSize .set 2
                .else
                    parameterSourceSize .set 1
                .endif
            .else ; not a constant, so just match destination size. 
                parameterSourceSize .set parameterDestSize
            .endif
    
        ; Check if source is using a size cast:
        .elseif .xmatch( .left( 1, {_T_SOURCE_}), {(} ) ; size cast
            .if .xmatch( .mid( 1, 1 , {_T_SOURCE_}) ,  .byte )
                castbase .set 1
            .elseif .xmatch( .mid( 1, 1 , {_T_SOURCE_}) ,  .word ) || .xmatch( .mid(1, 1 , {_T_SOURCE_}) ,  .addr )
                castbase .set 2
            .elseif .xmatch( .mid( 1, 1 , {_T_SOURCE_}) ,  .dword )
                castbase .set 4
            .else
                .error "Size expected for cast: .byte, .word, .addr or .dword"
                .fatal "STOP"
            .endif    
            
            .if .match( .mid( 2, 1 , {_T_SOURCE_}), 1) ; number
                castbase .set castbase * .mid(2, 1 , {_T_SOURCE_})
                .if .not .xmatch( .mid( 3, 1 , {_T_SOURCE_}), {)} )
                    .error "`)` expected."
                    .fatal "STOP"
                .endif
                .define _SOURCE_ .mid( 4, .tcount( {_T_SOURCE_} ) - 4 , {_T_SOURCE_})
            .else ; no number in cast
                .if .not .xmatch( .mid( 2, 1 , {_T_SOURCE_}), {)} )
                    .error "`)` expected."
                    .fatal "STOP"
                .endif
                .define _SOURCE_ .mid( 3, .tcount( {_T_SOURCE_} ) - 3 , {_T_SOURCE_})
            .endif
            parameterSourceSize .set castbase
        
        .else
            ; Start here: no leading tokens to skip due to not address operator, not immediate, not cast size:
            .define _SOURCE_  _T_SOURCE_
            
            ; is source a register?
            ; if not check for other operators supported:
            ; < for lowbyte
            ; > for high bye
            ; otherwise check for an identifier and find its size if possible
            
            ; default:
            ; size of ident may be difficult to determine, so default to this for ca65 to evaluate later and copy parameterDestSize number of bytes:
            parameterSourceSize .set parameterDestSize
                
            ; see if we can determine the size:
            
            ; Check if register:
            .if .xmatch(_SOURCE_,a) || .xmatch(_SOURCE_,x) || .xmatch(_SOURCE_,y)
                parameterSourceSize .set 1
                sourceIsReg .set 1
            .elseif .xmatch(_SOURCE_,ax) || .xmatch(_SOURCE_,ay) || .xmatch(_SOURCE_,xy)
                parameterSourceSize .set 2
                sourceIsReg .set 1
            ; Check if lowbyte or high byte operator: <, >
            .elseif .xmatch( {.left(1, {_SOURCE_} ) } , {<} ) || .xmatch( {.left(1, {_SOURCE_} ) } , {>} )
                parameterSourceSize .set 1
                
            ; see if we can get the size of an single token ident. ca65 is funny with scoping due to one pass, not much more can easily be done.
            ; could be more strict with checking ident. sizes, but it would require calls to use explicit scoping or casting idents.
            ; Comment out or remove the next 4 lines if it causes any trouble.
            .elseif .tcount( { _SOURCE_ } ) = 1 
                .ifdef  _SOURCE_ 
                    parameterSourceSize .set .sizeof(  _SOURCE_  )
                .endif
            .endif
        .endif
        
        ; error if source is bigger than dest.
        .if parameterSourceSize > parameterDestSize
            .error .sprintf( "Parameter overflow for '%s'. Parameter size: %d bytes. Passed value size: %d bytes.", _DEST_STR_, parameterDestSize, parameterSourceSize)
            .fatal "STOP"
        .endif
        
        ; check for register parameters not supported:
        .if destIsReg && sourceIsReg && ( parameterSourceSize > 1 || parameterDestSize > 1 )
            ; this is an error, unless register parameters match (copy/move not required)
            .if .not (.xmatch(_SOURCE_, _DEST_)) 
                .error .sprintf("Register parameter %s: Not supported with register source.", .string(_DEST_))
                .fatal "STOP"
            .endif
        .endif
        
        ; Now figure out how to load and store parameter data:
        
        .if pass = 0 ; first pass, look for copy of regs to memory only
            ; if source is a CPU reg. and destination is not a register: (assume it is memory)
            .if (.xmatch( _SOURCE_, a) || .xmatch( _SOURCE_, x) || .xmatch( _SOURCE_, y) ) && (.not destIsReg)
                .if .xmatch( _SOURCE_, a) ; if the source is reg.a:
                    sta _DEST_
                .elseif .xmatch( _SOURCE_, x) ; if the source is reg.x:
                    stx _DEST_
                .elseif .xmatch( _SOURCE_, y) ; if the source is reg.y
                    sty _DEST_
                .endif   
                ; check if _DEST_ memory size is bigger than one byte and warn
                .if parameterDestSize > 1
                    .warning "Register source for parameter that is larger than one byte. High bytes not set."
                .endif
            .elseif (.xmatch( _SOURCE_, ax) || .xmatch( _SOURCE_, ay) || .xmatch( _SOURCE_, xy) ) && (.not destIsReg)
                .if .xmatch( _SOURCE_, ax) ; if the source is ax:
                    sta _DEST_
                    stx _DEST_ + 1
                .elseif .xmatch( _SOURCE_, ay) ; if the source is ay:
                    sta _DEST_
                    sty _DEST_ + 1
                .elseif .xmatch( _SOURCE_, xy) ; if the source is xy:
                    stx _DEST_
                    sty _DEST_ + 1
                .endif   
            .endif
            
        .elseif pass = 1; end for first pass, start for second pass, look for reg to reg copies
            ; if the source is reg.a:
            .if .xmatch( _SOURCE_, a) && destIsReg
                ; check if reg.a has already been overwritten
                .if ___functionMacro::regAaccessedFlag
                    .error "Function parameter list requested source data from register a after it has been written."
                    .fatal "STOP"
                .endif        
                .if  .xmatch(_DEST_,x)  ; if destination is reg.x
                    tax
                    ___functionMacro::regXaccessedFlag .set 1
                .elseif .xmatch(_DEST_,y)  ; if destination is reg.y
                    tay
                    ___functionMacro::regYaccessedFlag .set 1
                .elseif .xmatch(_DEST_,a)  ; if destination is reg.a
                    ; just mark reg.a as used
                    ___functionMacro::regAaccessedFlag .set 1
                .endif
            
            ; if the source is reg.x:
            .elseif .xmatch( _SOURCE_, x) && destIsReg
                ; check if reg.x has already been overwritten
                .if ___functionMacro::regXaccessedFlag
                    .error "Function parameter list requested source data from register x after it has been written."
                    .fatal "STOP"
                .endif
                .if  .xmatch(_DEST_,a)  ; if destination is reg.a
                    txa
                    ___functionMacro::regAaccessedFlag .set 1
                .elseif .xmatch(_DEST_,y)  ; if destination is reg.y - error. We could use a temp for this, but better to get the user to work around it
                    .error "Function parameter list requested copy of register x to register y."
                    .fatal "STOP"
                .elseif .xmatch(_DEST_,x)  ; if destination is reg.x
                    ; just mark reg.x as used
                    ___functionMacro::regXaccessedFlag .set 1
                .endif
                
            ; if the source is reg.y:
            .elseif .xmatch( _SOURCE_, y) && destIsReg
                ; check if reg.y has already been overwritten
                .if ___functionMacro::regYaccessedFlag
                    .error "Function parameter list requested source data from register y after it has been written."
                    .fatal "STOP"
                .endif
                .if  .xmatch(_DEST_,a)  ; if destination is reg.a
                    tya
                    ___functionMacro::regAaccessedFlag .set 1
                .elseif .xmatch(_DEST_,x)  ; if destination is reg.x - error. We could use a temp for this, but better to get the user to work around it
                    .error "Function parameter list requested copy of register y to register x."
                .elseif .xmatch(_DEST_,y)  ; if destination is reg.y
                    ; just mark reg.y as used
                    ___functionMacro::regYaccessedFlag .set 1
                .endif            
            .elseif destIsReg && sourceIsReg && parameterSourceSize > 1 && parameterDestSize > 1 
                ; if we are here, source and dest. are the same, so just mark the registers as used.
                .if .xmatch( _DEST_, ax)
                    ___functionMacro::regAaccessedFlag .set 1
                    ___functionMacro::regXaccessedFlag .set 1
                .elseif .xmatch( _DEST_, ay)
                    ___functionMacro::regAaccessedFlag .set 1
                    ___functionMacro::regYaccessedFlag .set 1
                .elseif .xmatch( _DEST_, xy)
                    ___functionMacro::regXaccessedFlag .set 1
                    ___functionMacro::regYaccessedFlag .set 1
                .endif
            .endif
                
        .elseif pass = 2 ; end second pass; third pass, look for memory to memory
        
            .if (.not destIsReg ) && (.not sourceIsReg ) ; if destination and source is not a register, it is memory
                ; check which register is open for moving memory
                ; we don't have to mark it as used, because it is not holding a value that matters if not already marked
                .if .not ___functionMacro::regAaccessedFlag
                    ___moveMem a, _SOURCE_, _DEST_, parameterDestSize, parameterSourceSize, immMode
                .elseif .not ___functionMacro::regXaccessedFlag
                    ___moveMem x, _SOURCE_, _DEST_, parameterDestSize, parameterSourceSize, immMode
                .elseif .not ___functionMacro::regYaccessedFlag
                    ___moveMem y, _SOURCE_, _DEST_, parameterDestSize, parameterSourceSize, immMode
                .else
                    ; This is only possible if all registers are loaded as themselves.
                    .error "Function parameter list requested parameter memory copy with all registers used as parameters."
                    .fatal "STOP"
                .endif
                
            .endif    
            
        .elseif pass = 3 ; end third pass; start forth pass, look for copy of memory to reg.
        
            .if .xmatch( _DEST_, a) ; if the dest. is reg.a:
                .if .not sourceIsReg ; if source is not a register, assume it is memory
                    ; load reg.a, but mark it as written in case there is another attempt
                    .if .not ___functionMacro::regAaccessedFlag
                        ___functionMacro::regAaccessedFlag .set 1
                        .if immMode
                            lda # _SOURCE_
                        .else
                            lda _SOURCE_
                        .endif   
                    .else
                        .error "Function parameter list has already used register a."
                        .fatal "STOP"
                    .endif
                .endif
            .elseif .xmatch( _DEST_, x) ; if the dest. is reg.x:
                .if .not sourceIsReg ; if source is not a register, assume it is memory
                    ; load reg.x, but mark it as written in case there is another attempt
                    .if .not ___functionMacro::regXaccessedFlag
                        ___functionMacro::regXaccessedFlag .set 1
                        .if immMode
                            ldx # _SOURCE_
                        .else
                            ldx _SOURCE_
                        .endif
                    .else
                        .error "Function parameter list has already used register x."
                        .fatal "STOP"
                    .endif            
                .endif
            .elseif .xmatch( _DEST_, y) ; if the dest. is reg.y
                .if .not sourceIsReg ; if source is not a register, assume it is memory
                    ; load reg.y, but mark it as written in case there is another attempt
                    .if .not ___functionMacro::regYaccessedFlag
                        ___functionMacro::regYaccessedFlag .set 1
                        .if immMode
                            ldy # _SOURCE_
                        .else
                            ldy _SOURCE_
                        .endif
                    .else
                        .error "Function parameter list has already used register y."
                        .fatal "STOP"
                    .endif
                .endif  
            .elseif .xmatch( _DEST_, ax) ; if the dest. is reg.ax:
                .if .not sourceIsReg ; if source is not a register, assume it is memory
                    ; load reg.ax, but mark it as written in case there is another attempt
                    .if ( .not ___functionMacro::regAaccessedFlag ) && ( .not ___functionMacro::regXaccessedFlag )
                        ___functionMacro::regAaccessedFlag .set 1
                        ___functionMacro::regXaccessedFlag .set 1
                        .if immMode
                            lda # <_SOURCE_
                            ldx # >_SOURCE_
                        .else
                            lda _SOURCE_
                            ldx _SOURCE_ + 1
                        .endif   
                    .else
                        .error "Function parameter list has already used register a/x."
                        .fatal "STOP"
                    .endif
                .endif
            .elseif .xmatch( _DEST_, ay) ; if the dest. is reg.ay:
                .if .not sourceIsReg ; if source is not a register, assume it is memory
                    ; load reg.ay, but mark it as written in case there is another attempt
                    .if ( .not ___functionMacro::regAaccessedFlag ) && ( .not ___functionMacro::regYaccessedFlag )
                        ___functionMacro::regAaccessedFlag .set 1
                        ___functionMacro::regXaccessedFlag .set 1
                        .if immMode
                            lda # <_SOURCE_
                            ldy # >_SOURCE_
                        .else
                            lda _SOURCE_
                            ldy _SOURCE_ + 1
                        .endif   
                    .else
                        .error "Function parameter list has already used register a/y."
                        .fatal "STOP"
                    .endif
                .endif
            .elseif .xmatch( _DEST_, xy) ; if the dest. is reg.xy:
                .if .not sourceIsReg ; if source is not a register, assume it is memory
                    ; load reg.xy, but mark it as written in case there is another attempt
                    .if ( .not ___functionMacro::regXaccessedFlag ) && ( .not ___functionMacro::regYaccessedFlag )
                        ___functionMacro::regXaccessedFlag .set 1
                        ___functionMacro::regYaccessedFlag .set 1
                        .if immMode
                            ldx # <_SOURCE_
                            ldy # >_SOURCE_
                        .else
                            ldx _SOURCE_
                            ldy _SOURCE_ + 1
                        .endif   
                    .else
                        .error "Function parameter list has already used register x/y."
                        .fatal "STOP"
                    .endif
                .endif
            .endif    
        .endif ; end for forth pass
    .else
        ; here: comma_pos = -1 
        ; this indicates a blank parameter:
        ; define this so it matches up with the undefine below:
        .define _SOURCE_
    .endif
    
    .if parameterMode = 0
        ; no named parameters:
        ; increment parameterCounter for the next loop, and add parameter size to find the next memory location for the next parameter
        ___functionMacro::parameterCounter .set ___functionMacro::parameterCounter  + 1
        .if .not destIsReg 
            ___functionMacro::parameterAddress .set ___functionMacro::parameterAddress + parameterDestSize
        .endif
    .endif 
    
    .undefine _DEST_STR_
    .undefine _SOURCE_
    .undefine _DEST_
    .undefine _PARAM_
    .undefine _T_DEST_
    .undefine _T_SOURCE_

    ; Call again with next param if there was a comma found:
    .if comma_pos
        ; skip leading comma for blank parameter
        .if comma_pos = -1
            comma_pos .set 0
        .endif
        ___loadParams { .mid ( comma_pos + 1 , .tcount({param}) - comma_pos - 1, {param} ) }, pass, parameterMode
    .endif

.endmacro

; --------------------------------------------------------------------------------------------
; Function: call
;
; Parameters:
;
; funcname - name of the function to call.
; param - parameter list enclosed in curly brackets.
;
; Call a function with an optional parameter list.
;
; Two modes of calling:
;
; Each parameter must have a parameter name, defined when the function was declared or defined, 
; followed by a colon (:) and the value or source to load the parameter from.
; Parameters don't have to be in order and are optional.
;
; Or:
;
; Call with values to pass only, in the same order as the declaration. Values can be omitted,
; but will be undefined. 
; 
; Example: 
; ---code
; call myFunc, { a: #$23, foo: bar, x: #$20 }
; call myFunc, { #23, bar, #$20 }            ; same code output as first example
; call myFunc, { #23, , #$20 }               ; parameter foo is skipped
; ---

.macro call funcname, param

    .local callMode ; named or unnamed parameters?

    ; ca65 won't let you use .ident with scoped names, so we'll use this macro
    ; for finding: funcname___declared, funcname__params inside a scoped call
    ; it should work with any depth of scope
    ; assume funcname is scoped if it is more than 1 token:
    .if .tcount(funcname) > 1
        .define _FUNC(suffix) .left(.tcount(funcname) - 2, funcname)::.ident(.sprintf("%s%s", .string(.right(1,funcname)) , .string( suffix ) ))
    .else
        .define _FUNC(suffix) .ident(.sprintf("%s%s",.string(funcname), .string( suffix ) ))
    .endif

    ; make sure the function is declared, don't check if defined: It could be an imported function.
    .if .not .defined( _FUNC { ___declared } )
        .error "This function has not been declared or defined."
        .fatal "STOP"
    .endif
    
    ; check if there are any colons: ':' - ca65 won't match a double colon (::) here
    ; if none, we will call load parameters in parameter value only mode ( 0 ), and load them in order as they were declared.
    callMode .set 0
    ___findToken {param}, {:}, callMode
    
    ; Flags to check if a register was overwritten. Clear the flags first
    ___functionMacro::regAaccessedFlag    .set 0
    ___functionMacro::regXaccessedFlag    .set 0     
    ___functionMacro::regYaccessedFlag    .set 0
    
    ; Scan the parameters
    ; Function name is accessed by ___loadParams with _FUNC to use the proper scope to use when checking parameters
    ; Four passes, 1) copy registers to memory, 2) copy reg to reg, 3) copy memory to memory, 4) copy memory to registers
    ; The only real problem with the 4 passes is if all registers are loaded as themselves and there are more parameters to load
    
    .repeat 4, i
        ; keep track of where to load parameters in value only parameter mode
        ___functionMacro::parameterAddress .set _FUNC { ___params } :: parameterMemoryStart
        ; count parameters in order they are declared
        ___functionMacro::parameterCounter .set 0
        ___loadParams {param}, i, callMode
    .endrepeat
   
    
    .undefine _FUNC
    
    ; warning for information about nested calls:
    .if ::_CALL_WARNINGS_
        .if ___functionMacro::functionBodyActive
            .out .sprintf("Nested call to %s from function %s", .string(funcname), .string(_ACTIVE_DEFINE_FUNCTION_))
        .endif
    .endif
   
    jsr funcname
    ndxLuaExecStrDebug .sprintf ( "returnFromFunc ('%s')", .string(.right(1,funcname))), 1
    
.endmacro

; --------------------------------------------------------------------------------------------
; sub macro to evaluate parameters when declaring or defining a function
; if function name was passed, verify the parameters match the declaration

.macro ___func_evalparams param, funcname

    .if .blank ({param})
        ; nothing to do
        .exitmacro
    .endif

    .local comma_pos ; position of comma in token list
    .local colon_pos ; position of colon in single parameter token list
    .local size      ; size of memory to be reserved   
    .local reg_value
    colon_pos .set 0
    comma_pos .set 0    
    
    ; find comma in param list. It's okay to not find one if there is only one parameter
    ___findToken {param}, {,}, comma_pos

    ; define and then work with one parameter at a time
    .if comma_pos
        .define _PARAM_  .mid(0, comma_pos, {param})
    .else
        .define _PARAM_ param
    .endif
    
    ; The parameter should be an identifier and a size
    .if .tcount( { _PARAM_ } ) <> 2
        .error "Identifier and size value expected in parameter list."
        .fatal "STOP"
    .endif
    
    .define _PARAM_IDENT_  .left(1,  _PARAM_  )
    .define _PARAM_SIZE_  .right(1,  _PARAM_ )
    
    ; At this point _PARAM_ should hold an identifier and a size/type
    ; NOTE - we are in a scope named the same as the function
    
    ; if function name was passed, just check the identifier already exists
    ; .. this is for verifying a declared function when defining it
    .if .not .blank(funcname)
        .if .not .defined ( .ident(.concat(.string(funcname), "___params"))::.ident(.string(_PARAM_IDENT_) ) )
          .error .sprintf( "Parameter name for function does not match declaration: %s", .string(_PARAM_IDENT_))
          .fatal "STOP"
        .endif
    .endif
    
    ; check if parameter 'size' is a register:
    ; set bit8 for 'location' to indicate a register, since parameters must be in zeropage, this value won't conflict with any others
    .if .xmatch(_PARAM_SIZE_,  a) || .xmatch(_PARAM_SIZE_,  x) || .xmatch(_PARAM_SIZE_,  y) || \
        .xmatch(_PARAM_SIZE_,  ax) || .xmatch(_PARAM_SIZE_,  ay) || .xmatch(_PARAM_SIZE_,  xy)
        .if .xmatch(_PARAM_SIZE_,  a  )
            reg_value = (1 << 8) | 0 ; indicate reg.a
            ___functionMacro::regAaccessedFlag .set ___functionMacro::regAaccessedFlag + 1
        .elseif .xmatch(_PARAM_SIZE_,  x) 
            reg_value = (1 << 8) | 1 ; indicate reg.x
            ___functionMacro::regXaccessedFlag .set ___functionMacro::regXaccessedFlag + 1
        .elseif .xmatch(_PARAM_SIZE_,  y)
            reg_value = (1 << 8) | 2 ; indicate reg.y
            ___functionMacro::regYaccessedFlag .set ___functionMacro::regYaccessedFlag + 1
        .elseif .xmatch(_PARAM_SIZE_,  ax) 
            reg_value = (1 << 8) | 3 ; indicate reg.ax
            ___functionMacro::regAaccessedFlag .set ___functionMacro::regAaccessedFlag + 1
            ___functionMacro::regXaccessedFlag .set ___functionMacro::regXaccessedFlag + 1
        .elseif .xmatch(_PARAM_SIZE_,  ay)
            reg_value = (1 << 8) | 4 ; indicate reg.ay
            ___functionMacro::regAaccessedFlag .set ___functionMacro::regAaccessedFlag + 1
            ___functionMacro::regYaccessedFlag .set ___functionMacro::regYaccessedFlag + 1
        .elseif .xmatch(_PARAM_SIZE_,  xy)
            reg_value = (1 << 8) | 5 ; indicate reg.xy
            ___functionMacro::regXaccessedFlag .set ___functionMacro::regXaccessedFlag + 1
            ___functionMacro::regYaccessedFlag .set ___functionMacro::regYaccessedFlag + 1
        .endif
            
        
        .if (___functionMacro::regAaccessedFlag > 1) || (___functionMacro::regXaccessedFlag > 1) || (___functionMacro::regYaccessedFlag > 1)
            .error "Duplicate register use."
            .fatal "STOP"
        .endif
        
        .if .not .blank(funcname)
            .if reg_value <> .ident(.concat(.string(funcname), "___params"))::.ident(.string(_PARAM_IDENT_) )
                .error "Parameters for function do not match declaration. Register mismatch."
                .fatal "STOP"
            .endif
        .else
            .ident(.string(_PARAM_IDENT_)) = reg_value
            ;also add an array element, indexed by counter
            ___paramListSizes { ___functionMacro::parameterCounter } = reg_value
        .endif
    .else
        ; not a register:    
        ; figure out the size of the parameter:
        .if .xmatch(_PARAM_SIZE_,  .byte  )
            size .set 1
        .elseif .xmatch(_PARAM_SIZE_,  .word  ) || .xmatch(_PARAM_SIZE_,  .addr  )       
            size .set 2
        .elseif .xmatch(_PARAM_SIZE_,  .dword  )
            size .set 4
        .else
            .error "Size for parameter not recognized."
            .fatal "STOP"
        .endif
        
        ; verify the size if we are defining a function in the same source file as a declaration
        .if .not .blank(funcname)
            .if ( size <> .sizeof( .ident(.concat(.string(funcname), "___params"))::.ident(.string(_PARAM_IDENT_) )) ) || ( .ident(.concat(.string(funcname), "___params"))::.ident(.string(_PARAM_IDENT_))  <> ___functionMacro::parameterAddress) 
                .error .sprintf ("Parameter %s does not match declaration. Size mismatch.", .string(_PARAM_IDENT_))
                .fatal "STOP"
            .endif
        .endif
        ; create the parameter:
        
        ; decrease offset for next parameter - BEFORE we create the ident.
        .if ___functionMacro::libraryMode
            ___functionMacro::parameterAddress .set ___functionMacro::parameterAddress - size
        .endif
        
        ; create the identifier that we can check for later when loading parameters
        ; use a unnamed struct to define it so we can use .res to generate a size and it won't output any code
        ; (skip this if just verifying a declaration)
        .if .blank(funcname)
            .struct
                ; pad the struct with the offset
                .if ___functionMacro::parameterAddress > 0
                    .res ___functionMacro::parameterAddress
                .endif
                ; create the ident 
                .ident(.string(_PARAM_IDENT_)) .res size
            .endstruct
            ;add an array element, indexed by parameter address, that also holds the size:
            ___paramListSizes { ___functionMacro::parameterCounter } = size 
        .endif
        
        ; increase offset for next parameter - AFTER we create the ident.
        .if .not ___functionMacro::libraryMode
            ___functionMacro::parameterAddress .set ___functionMacro::parameterAddress + size
        .endif
        ___functionMacro::paramSizeTotal .set ___functionMacro::paramSizeTotal + size
    .endif
    
    ; increment the counter
    ___functionMacro::parameterCounter .set ___functionMacro::parameterCounter + 1
    
    .undefine _PARAM_IDENT_
    .undefine _PARAM_SIZE_
    .undefine _PARAM_

    ; if there was a comma, do again, removing the processed parameter:
    .if comma_pos
        ___func_evalparams { .mid ( comma_pos + 1 , .tcount({param}) - comma_pos - 1, {param} ) }, funcname
    .endif

.endmacro

; --------------------------------------------------------------------------------------------
; Function: declarefunc
;
; Forward declaration of a function with an optional parameter list.
; This can be used to declare a function so it can be called before it is defined in the same source file,
; or when importing a function from another library. 
; Optionally, the offsets for the function's parameter memory can be changed with the value passed in base.
;
; Parameters:
;
;   funcname - The name of the function/procedure/routine. Required.
;
;   params - The parameter list enclosed in { }. Optional.
;
;   base - The amount of parameter memory to skip when defining this function. Optional.
; 
; Example: 
; ---code
; declarefunc draw, { xloc .byte, yloc .byte, index x }, 7
; ---

.macro declarefunc funcname, params, base

    .local _base

    ; check if this scope has already been declared
    .if .defined(.ident(.concat(.string(funcname), "___declared")))
        .error "This function has already been declared".
        .fatal "STOP"
    .endif
    
    ; where to start marking parameter offsets
    .ifnblank base
        _base .set base
    .else
        _base .set 0
    .endif
    
    .if ___functionMacro::libraryMode
        ; in library mode, start at the end of parameter memory and count down:
        ___functionMacro::parameterAddress .set ::_PARAMETER_MEMORY_MAXSIZE_ + ::_PARAMETER_MEMORY_START_ - _base
    .else
        ___functionMacro::parameterAddress .set ::_PARAMETER_MEMORY_START_ + _base
    .endif    
    
    ; keep track of register usage
    ___functionMacro::regAaccessedFlag .set 0
    ___functionMacro::regXaccessedFlag .set 0
    ___functionMacro::regYaccessedFlag .set 0
    
    ; track memory needed for parameters
    ___functionMacro::paramSizeTotal .set 0
    ; count parameters in order they are declared
    ___functionMacro::parameterCounter   .set 0
    
    ; each function needs a scope to track parameters. 
    .scope .ident(.concat(.string(funcname), "___params"))
        ; save parameter memory start:
        
        ; if library mode, load parameters at the end of parameter memory
        .if ___functionMacro::libraryMode
            ; in library mode the start address is the parameterAddress AFTER evaluation
            ___func_evalparams {params}
            parameterMemoryStart .set ___functionMacro::parameterAddress
            parameterMemorySize .set ___functionMacro::paramSizeTotal
        .else
            parameterMemoryStart = ___functionMacro::parameterAddress
            ___func_evalparams {params}
            ; save parameter memory size
            parameterMemorySize = ___functionMacro::paramSizeTotal
        .endif
        
        ; create this, but set to 0 for now
        localMemorySize .set 0
        
        .assert  ( parameterMemoryStart + parameterMemorySize ) <= ( ::_PARAMETER_MEMORY_START_ + ::_PARAMETER_MEMORY_MAXSIZE_ ), error, .sprintf( "Parameters take too much space in function %s", .string(funcname) )
    .endscope

    ; create an identifier to mark this as declared. it would be nice to create this inside the scope above, but it
    ; makes it possible to determine that the scope above exists.
    .ident(.concat(.string(funcname), "___declared")) = 1
    
    ; .global will import this function if it is not defined later in the same source file:
    ; (skip this step if the function is being defined without a declaration)
    
    .if ___functionMacro::global
        .global funcname
    .endif

.endmacro

; --------------------------------------------------------------------------------------------
; Function: func
;
; Definition of a function with an optional parameter list.
;
; Parameters:
;
;   funcname - The name of the function/procedure/routine. Required.
;
;   params - The parameter list enclosed in { }. Optional.
;
;   base - The amount of parameter memory to skip when defining this function. Optional.
;
; This starts the function body. The parameter list must match the parameter list of
; the previously declared function if one exists. 
;
; Optionally, if the function has not been declared previously, the offsets for the function's parameter 
; memory can be changed with the value passed in base. if the declaration has already been made with base, 
; don't define it again.
;
; Parameters can be accessed in the function definition by using the keyword *param*
; before the symbol name.
; 
; Example: 
; ---code
; func myfunc, { dataPtr .addr, index x }
;
; lda (param dataPtr), x
; ; etc ..
;
; endfunc
; --- 

.macro func funcname, params, base

    .if ___functionMacro::functionBodyActive
        .error "Nested function definitions not supported"
        .fatal "STOP"
    .endif
    
    ; Was this function already declared/defined?
    .if .defined(.ident(.concat(.string(funcname), "___declared")))
        .if .defined(.ident(.concat(.string(funcname), "___params"))::defined)
            .error "This function already exists."
            .fatal "STOP"
        .endif
        ; if this function has been declared, its parameter memory has already been setup
        .if .not .blank(base)
            .error "Function is previously declared. Cannot change parameter offsets after function declaration."
            .fatal "STOP"
        .endif
    .endif
    
    ; has it been declared already?
    ; if so, make sure parameters match
    .if .defined(.ident(.concat(.string(funcname), "___declared")))
        ; find any offset
        ___functionMacro::parameterAddress .set .ident(.concat(.string(funcname), "___params"))::parameterMemoryStart
        
        ; keep track of register usage
        ___functionMacro::regAaccessedFlag .set 0
        ___functionMacro::regXaccessedFlag .set 0
        ___functionMacro::regYaccessedFlag .set 0
        
        ___func_evalparams {params}, funcname
        
    .else ; not declared already, so declare it, but no need to export/import
        ___functionMacro::global .set 0
        declarefunc funcname, {params}, {base}
        ___functionMacro::global .set 1
    .endif
        
    .define _ACTIVE_DEFINE_FUNCTION_ funcname
    
    ; make parameter and local var access easy:
    
    ; Function: param
    ; 
    ; Macro to access parameters when in the function body.
    
    .define param(p) .ident(.concat(.string( funcname ), "___params"))::p
    
    ; Function: local
    ; 
    ; Macro to access locals when in the function body.
    
    .define local(p) .ident(.concat(.string( funcname ), "___locals"))::p
    
     ; mark that we are in a function body
     ___functionMacro::functionBodyActive .set 1
     ; create a new scope/label for the function code:
    .proc funcname
    
    ; add parameters used to LUA
      ; in library mode, the Lua script will evaluate some conditions differently 
    ndxLuaExecStrDebug .sprintf ( "functionEntry ('%s', %d, %d, %d) ", .string(funcname), .ident(.concat(.string(funcname), "___params"))::parameterMemoryStart, .ident(.concat(.string(funcname), "___params"))::parameterMemorySize,  ___functionMacro::libraryMode), 1
   
.endmacro


; --------------------------------------------------------------------------------------------
; Function: endfunc
;
; Ends a function definition.

.macro endfunc

    .if .not ___functionMacro::functionBodyActive 
        .error "`endfunc` with no `func.`"
        .fatal "STOP"
    .endif

    .endproc
    
    ___functionMacro::functionBodyActive .set 0
    .ident(.concat(.string(_ACTIVE_DEFINE_FUNCTION_), "___params"))::defined = 1
    .undefine param
    .undefine local
    .undefine _ACTIVE_DEFINE_FUNCTION_ 
.endmacro

; --------------------------------------------------------------------------------------------
; Function: locals
;
; Begin definition of local variables.
;
; Local variables use the same memory space reserved for parameters.
;
; Locals can be accessed in the function definition by using the keyword *local*
; before the symbol name.
; 
; Example: 
; ---code
; func myfunc, { xloc .byte, yloc .byte, index x }
;
;   locals
;     counter .byte
;     counterPtr  .addr
;   endfunc
;
;   lda #<local counter
;   sta local counterPtr
;   lda #>local counter
;   sta local counterPtr + 1
;   ; ....etc
;
; endfunc 
; --- 

.macro locals _size

    .if .not ___functionMacro::functionBodyActive
        .error "Locals must be defined inside the function body."
        .fatal "STOP"
    .endif
    
    .if ___functionMacro::libraryMode
        .ifnblank _size
            .define __BASE__ .ident(.concat(.string(_ACTIVE_DEFINE_FUNCTION_), "___params"))::parameterMemoryStart - _size
            .define __LOCAL_SIZE__  _size
        .else
            .error "Size required for locals in library mode."
            .fatal "STOP"
        .endif
    .else      
        ; symbols inside struct start at 0
        ; reserve the size of the parameters used to start at the correct offset
        .define __BASE__  .ident(.concat(.string(_ACTIVE_DEFINE_FUNCTION_), "___params"))::parameterMemorySize + .ident(.concat(.string(_ACTIVE_DEFINE_FUNCTION_), "___params"))::parameterMemoryStart
    .endif 
    
    .struct .ident( .concat(.string(_ACTIVE_DEFINE_FUNCTION_), "___locals" ))  
        .if __BASE__ > 0
            .res  __BASE__
        .endif    

    
.endmacro

; --------------------------------------------------------------------------------------------
; Function: endlocals
;
; End definition of local variables.

.macro endlocals

    .endstruct
    
    .ident(.concat(.string(_ACTIVE_DEFINE_FUNCTION_), "___params"))::localMemorySize .set .sizeof( .ident( .concat(.string(_ACTIVE_DEFINE_FUNCTION_), "___locals" )))  - ( __BASE__ )
    ; size of locals is the size of the structure less the reserved amount in the structure (__BASE__):
    ; localsSize =  .sizeof( .ident( .concat(.string(_ACTIVE_DEFINE_FUNCTION_), "___locals" ))) -  __BASE__ 
    ; The largest offset used then is localsSize + __BASE__, which is just :
    .assert .sizeof( .ident( .concat(.string(_ACTIVE_DEFINE_FUNCTION_), "___locals" ))) <= (::_PARAMETER_MEMORY_START_ + ::_PARAMETER_MEMORY_MAXSIZE_ ), error, .sprintf( "Parameters/Locals take too much space in function %s", .string(_ACTIVE_DEFINE_FUNCTION_) )
    .if ___functionMacro::libraryMode
        .assert .ident(.concat(.string(_ACTIVE_DEFINE_FUNCTION_), "___params"))::localMemorySize = __LOCAL_SIZE__, error, .sprintf( "Locals size mismatch for library mode function %s", .string(_ACTIVE_DEFINE_FUNCTION_) )
        .ident(.concat(.string(_ACTIVE_DEFINE_FUNCTION_), "___params"))::parameterMemoryStart .set .ident(.concat(.string(_ACTIVE_DEFINE_FUNCTION_), "___params"))::parameterMemoryStart - __LOCAL_SIZE__
        .undefine __LOCAL_SIZE__
    .endif
    
    .undefine __BASE__
    
    ; tell Lua about our locals, adjust parameterMemoryStart for libraryMode
    ndxLuaExecStrDebug .sprintf ( "addLocalMemory ('%s', %d, %d)", .string(_ACTIVE_DEFINE_FUNCTION_), .ident(.concat(.string(_ACTIVE_DEFINE_FUNCTION_), "___params"))::localMemorySize, .ident(.concat(.string(_ACTIVE_DEFINE_FUNCTION_), "___params"))::parameterMemoryStart)
    
.endmacro


; --------------------------------------------------------------------------------------------
; Function: funcStartLibraryMode
;
; Begin allocating parameter/local memory from the end of the memory space to mitigate parameter conflicts when
; the function is called.

.macro funcStartLibraryMode
    ___functionMacro::libraryMode .set 1
.endmacro

; --------------------------------------------------------------------------------------------
; Function: funcEndLibraryMode
; Return to allocating parameter/local memory from the start of the memory space.

.macro funcEndLibraryMode
    ___functionMacro::libraryMode .set 0
.endmacro

; --------------------------------------------------------------------------------------------
; Function: funcReleaseMemory
; Memory in the currently active function will me marked as released for the Lua script and can be used 
; in a called function without a warning.

.macro funcReleaseMemory
    ndxLuaExecStrDebug .sprintf ( "releaseMemory ('%s')", .string(_ACTIVE_DEFINE_FUNCTION_))
.endmacro

; --------------------------------------------------------------------------------------------
; Function: funcReserveMemory
; Memory for the currently active function will me marked as in use for the Lua script and warnings will
; again be issued for any use in a called function.

.macro funcReserveMemory
    ndxLuaExecStrDebug .sprintf ( "reclaimMemory ('%s')", .string(_ACTIVE_DEFINE_FUNCTION_))
.endmacro

; --------------------------------------------------------------------------------------------
; Function: funcTailCall
; Tell the Lua script that we called another function without returning from the currently active
; function. The call still needs to be eventually returned from for this to work properly. If not 
; returning from a call, use funcTrackingReset

.macro funcTailCall
    ndxLuaExecStrDebug .sprintf ( "tailCall ('%s')", .string(_ACTIVE_DEFINE_FUNCTION_))
.endmacro

; --------------------------------------------------------------------------------------------
; Function: funcCallTrackingOff
; Turn off all tracking via Lua script.

.macro funcCallTrackingOff
    ndxLuaExecStrDebug "functionCallTrackingToggle(0)"
.endmacro

; --------------------------------------------------------------------------------------------
; Function: funcCallTrackingOn
; Turn on all tracking via Lua script.

.macro funcCallTrackingOn
    ndxLuaExecStrDebug "functionCallTrackingToggle(1)"    
.endmacro

; --------------------------------------------------------------------------------------------
; Function: funcTrackingReset
; Reset all function tracking values for Lua script.

.macro funcTrackingReset
    ndxLuaExecStrDebug "resetFunctionTracking ()"
.endmacro

; --------------------------------------------------------------------------------------------
; Function: funcCallTrackingVerbose
; Toggle information about function calls 
; 1 -> Show a lot of information (slows down emulation)
; 0 -> Warn of problems only

.macro funcCallTrackingVerbose i
    ndxLuaExecStrDebug .sprintf("functionCallTrackingVerbose(%d)", i)
.endmacro


.endif
