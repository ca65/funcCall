; --------------------------------------------------------------------------------------------
; File: display.s
; --------------------------------------------------------------------------------------------
; Code execution continues here after the reset routine is completed.
;
; Tests and examples for funcCall.h macros and funcCall.lua

; --------------------------------------------------------------------------------------------
; Includes:

.include "config.h"
.include "header/nes_constants.h"
.include "header/funcCall.h"
.include "header/ppu.h"
.include "header/nmi.h"
.include "header/reset.h"
.include "header/controller.h"
.include "header/string.h"
.include "header/dis.h"

; --------------------------------------------------------------------------------------------

.segment "CODE"

; --------------------------------------------------------------------------------------------
.scope display


; --------------------------------------------------------------------------------------------
; Forward declarations: (uses .global)

declarefunc displayStart
declarefunc test1
declarefunc test2
declarefunc test3
declarefunc test4
declarefunc test5
declarefunc test6
declarefunc test7
declarefunc test8
declarefunc test9
declarefunc test10
declarefunc test11
declarefunc test12
declarefunc disSelf
declarefunc wait


.pushseg
.segment "RODATA"

    palette: .byte $0f,$00,$10,$30,$0f,$01,$21,$31,$0f,$06,$16,$26,$0f,$09,$19,$29

.segment "BSS"

    disString:      .res 20
    loopCounter:    .res 1
    screencount:    .res 3
    funcCallVerboseMode: .res 1
    
.popseg

; --------------------------------------------------------------------------------------------
;
; Wait for next frame: suppress all function tracking during this.
; Also check controller for B pressed to toggle verbose mode

func waitNMI_ReadPad
    call nmi::waitForNMI
    call controller::readPort, { player: #0 }
    lda controller::pressedNew
    and #BUTTON_B
    beq exit
        lda #$01
        eor funcCallVerboseMode
        sta funcCallVerboseMode
        beq :+
            funcCallTrackingVerbose 1
            ndxLuaExecStr "print('funcCallTrackingVerbose 1' )"
        bne exit
        :
        funcCallTrackingVerbose 0
        ndxLuaExecStr "print('funcCallTrackingVerbose 0' )", 1
    exit:

    rts
endfunc

.macro waitNMI
    funcCallTrackingOff ; suppress tracking of calls waiting and during NMI thread
    call waitNMI_ReadPad    
    funcCallTrackingOn
.endmacro

; --------------------------------------------------------------------------------------------
; for numbering the screens displayed: Inc the two byte string counter.

func IncScreenCount 
    
    ldx screencount + 1
    inx 
    cpx #('9' + 1 )
    bcc :+
        ldx #'0'
        inc screencount
    :
    stx screencount + 1
    rts

endfunc

; --------------------------------------------------------------------------------------------
; after reset, enter here:
; --------------------------------------------------------------------------------------------

setEntryFromReset

; --------------------------------------------------------------------------------------------

func displayMain
    
    ; zero end of string for str::print
    lda #$00
    sta screencount + 2
    ; maximum col for text with jPrint macro
    
    jPrintSetMaxCol 30 ; set max col for jPrint
    
    call displayStart    
    
    repeat:
        ; reset screencount
        ldx #'0'
        stx screencount 
        inx
        stx screencount + 1
        
        call test1
        call wait
        call test2
        call wait
        call test3
        call wait
        call test4
        call wait
        call test5
        call wait
        call test6
        call wait
        call test7
        call wait
        call test8
        call wait
        call test9
        call wait
        call test10
        call wait
        call test11
        call wait
        call test12
        call wait        
        call disSelf
        call wait
    jmp repeat    
    
    
endfunc

; --------------------------------------------------------------------------------------------
; Function: wait
; Turn on the screen, wait for A press
; turn off screen and clear it.

func wait

    call ppu::transferVramBuffer
    
    call str::print, { #28, #2, &screencount }
    call IncScreenCount
    
    wait_skip_start:
    
    puts 7, 27, "Press Button A..."
    PPU_RENDER_ON
    
    waitloop:
        waitNMI
        lda controller::pressedNew
        and #BUTTON_A
    beq waitloop
    
    PPU_RENDER_OFF
    waitNMI
    call ppu::clearNameTable, { #>NAME_TABLE_0_ADDRESS, #0, #0 }
    
    rts
    
endfunc

; --------------------------------------------------------------------------------------------

func displayStart

    jPrintSetCursor 3, 4
    jPrint "Examples using the funcCall.h macro system to define and call functions while tracking parameter and local variable usage and tracing and verifying function entry and return."
    
    call ppu::transferVramBuffer
    jPrint
    jPrint "Please use NintendulatorDX for best results."
    jPrint
    jPrint "Press B:"
    jPrint
    jPrint "Toggle verbose mode to log all function calls in the NDX Lua console. Warnings will still be displayed."
    jPrint "Call logs are turned off for the NMI thread."
    
    call ppu::transferVramBuffer
    
    ; these calls to setBgPalette all do the same thing:
    
    setPalLabel:
    call ppu::setBgPalette, { &palette }
    
    setPalLabel2:
    call ppu::setBgPalette, { a: #<palette, x: #>palette }
    
    setPalLabel3:
    lda #<palette
    ldx #>palette
    call ppu::setBgPalette, { ax }
    
    call ppu::updateBgPalette
    call ppu::transferVramBuffer
    
    ; no 'funcTailCall' here because we are not directly entering another tracked function 
    ; defined by 'func', but jumping to the middle of the function body.
    ; 'funcTailCall' is only useful for calling or falling through to another 'func' entry.
    jmp wait::wait_skip_start
    

endfunc

; --------------------------------------------------------------------------------------------

func test1

    jPrintSetCursor 3, 8
    jPrint "Very brief description of defining function parameters:"
    jPrint
    jPrint "func fun1, {foo .byte, \"
    jPrint "              bar .word}"
    jPrint
    jPrint "    (locals and function body would follow..)"
    jPrint 
    jPrint
    jPrint "Defines a function with a byte and a word parameter."

    rts
    
endfunc

; --------------------------------------------------------------------------------------------

func fun1, { foo .byte , bar .word }
    rts
endfunc

func test2

    locals
        line   .byte
    endlocals
    
    jPrintSetCursor 3, 6
    jPrint "Parameter passing."
    jPrint "'Named' parameters:"
    jPrint
    jPrint "call fun1, { foo: #$34, \"
    jPrint "            bar: #$2277 }"
    jPrint
    jPrint "Disassembly:"
    
    funcReleaseMemory   ; it's okay to overwrite temp memory
    
    fun1_label:
    call fun1, { foo: #$34, bar: #$2277 }
    
    funcReserveMemory   ; make sure temp memory is not used
    
    lda #15
    sta local line
    
    ; mark start of disassembly:
    call dis::setAddress, { &fun1_label }
    
    :
        inc local line
        call dis::disassemble, { &disString }
        pha
        call str::print, { #7, local line, &disString }
        pla
        
    cmp #$20 ;last instruction JSR
    bne :-
    
    rts

endfunc

; --------------------------------------------------------------------------------------------

func test3

    locals
        line   .byte
    endlocals
    
    jPrintSetCursor 3, 6
    jPrint "Parameter passing."
    jPrint "Values only:"
    jPrint
    jPrint "call fun1, { #$34, #$2277 }"
    jPrint
    jPrint "Disassembly:"

    funcReleaseMemory   ; it's okay to overwrite temp memory
    
    fun1_label:
    call fun1, { #$34, #$2277 }
    
    funcReserveMemory   ; make sure temp memory is not used
    
    lda #15
    sta local line
    
    ; mark start of disassembly:
    call dis::setAddress, { &fun1_label }
    
    :
        inc local line
        call dis::disassemble, { &disString }
        pha
        call str::print, { #7, local line, &disString }
        pla
        
    cmp #$20 ;last instruction JSR
    bne :-
    
    rts

endfunc

; --------------------------------------------------------------------------------------------

func test4

    locals
        line   .byte
    endlocals

    param1 = $302

    jPrintSetCursor 3, 6
    jPrint "Parameter passing."
    jPrint "Variables:"
    jPrint
    jPrint "param1 = $302"
    jPrint
    jPrint "call fun1, \"
    jPrint "    { #$55, param1 + 1 }"
    jPrint
    jPrint "Disassembly:"
    
    funcReleaseMemory   ; it's okay to overwrite temp memory
    
    fun1_label:
    call fun1, { #$55, param1 + 1 }
    
    funcReserveMemory   ; make sure temp memory is not used
    
    lda #15
    sta local line
    
    ; mark start of disassembly:
    call dis::setAddress, { &fun1_label }
    
    :
        inc local line
        call dis::disassemble, { &disString }
        pha
        call str::print, { #7, local line, &disString }
        pla
        
    cmp #$20 ;last instruction JSR
    bne :-
    
    rts
endfunc

; --------------------------------------------------------------------------------------------

func test5

    locals
        line   .byte
    endlocals

    jPrintSetCursor 3, 6
    jPrint "Parameter passing."
    jPrint "Address:"
    jPrint
    jPrint "call ppu::setBgPalette, \"
    jPrint "          { &palette }"
    jPrint
    jPrint "Disassembly:"
    
    lda #15
    sta local line
        
    ; mark start of disassembly:
    call dis::setAddress, { &displayStart::setPalLabel }
    :
        inc local line
        call dis::disassemble, { &disString }
        pha
        call str::print, { #7, local line, &disString }
        pla
        
    cmp #$20 ;last instruction JSR
    bne :-
    
    rts
endfunc

; --------------------------------------------------------------------------------------------

func test6

    locals
        line   .byte
    endlocals

    jPrintSetCursor 3, 6
    jPrint "Parameter passing."
    jPrint "Registers:"
    jPrint 
    jPrint "call ppu::setBgPalette, \"
    jPrint  " {a:#<palette, x:#>palette}"
    jPrint
    jPrint "Disassembly:"
    
    lda #15
    sta local line
    
    ; mark start of disassembly:
    call dis::setAddress, { &displayStart::setPalLabel2 }
    
    :
        inc local line
        call dis::disassemble, { &disString }
        pha
        call str::print, { #7, local line, &disString }
        pla
        
    cmp #$20 ;last instruction JSR
    bne :-
    
    rts
endfunc

; --------------------------------------------------------------------------------------------

func test7

    locals
        line   .byte
    endlocals

    jPrintSetCursor 3, 5
    jPrint "Parameter passing."
    jPrint "Registers pre-loaded:"
    jPrint 
    jPrint  "(parameter could be omitted)"
    jPrint
    jPrint "lda #<palette"
    jPrint "ldx #>palette"
    jPrint  "call ppu::setBgPalette, \"
    jPrint  "            { pal: ax }"
    jPrint
    jPrint "Disassembly:"
    
    lda #17
    sta local line
    
    ; mark start of disassembly:
    call dis::setAddress, { &displayStart::setPalLabel3 }
    
    :
        inc local line
        call dis::disassemble, { &disString }
        pha
        call str::print, { #7, local line, &disString }
        pla
        
    cmp #$20 ;last instruction JSR
    bne :-
    
    rts
    
endfunc

; --------------------------------------------------------------------------------------------

func callSecond, { foo .byte, bar .dword }
    rts
endfunc

func callFirst, { foo .byte, bar .dword, release .byte }

    lda param release
    beq :+
        funcReleaseMemory
    nop
    :
    call callSecond, { #$12, #$3456 }
    rts
endfunc


func test8

    jPrintSetCursor 3, 10
    jPrint "Function call with conflicting memory usage. NDX Lua console warning."
    jPrint
    jPrint "See source file:"
    jPrint "'func test8'"
    
    call callFirst, { , , #$00 }
    rts
endfunc

func test9

    jPrintSetCursor 3, 10
    jPrint "Function call with conflicting memory usage, but memory released. No warnings in console output."
    jPrint
    jPrint "See source file:"
    jPrint "'func test9'"
    
    call callFirst, { , , #$01 }
    rts
endfunc

; --------------------------------------------------------------------------------------------

func fallThrough, { doTailCall .byte }

    lda param doTailCall
    beq :+
        funcTailCall
    nop
    :
    ; fall through or uncomment the line below: it works too
    ; call fallThoughtoThis
endfunc

func fallThoughtoThis
    rts
endfunc


func test10

    jPrintSetCursor 3, 10
    jPrint "Function call with fall through and RTS from another function. NDX Lua console warning."

    call fallThrough, { #0 }
    rts
endfunc

func test11

    jPrintSetCursor 3, 10
    jPrint "Function call with fall through and RTS from another function, but 'funcTailcall' declared before. No warning, but the log in verbose mode will show the tail call."
    
    call fallThrough, { #1 }
    rts
endfunc

; --------------------------------------------------------------------------------------------

func manyParamTest, { foo xy, bar .word, baz .word, regA a, zaz .byte}

    rts
endfunc

func test12 

    locals
        line    .byte
    endlocals

    jPrintSetCursor 3, 3
    jPrint "Function call with many parameters, loading order optimized. Reg. A loaded, so X used to move values."
    jPrint "See source file:"
    jPrint "'func test12'"

    call ppu::transferVramBuffer
    
    funcReleaseMemory
    
    ; make a fake variable with address $200 (this way it will be assigned a size)
    .struct
        .res $200
        thisParam .res 2
    .endstruct
 
    lda #$01
    ldx #$01
    ldy #$01
    manyTest:
    call manyParamTest, { #$12, xy, thisParam, y, a }
    
    funcReserveMemory
    
    ; mark start of disassembly:
    call dis::setAddress, { &manyTest }
    
    lda #10
    sta local line
    
    :
        inc local line
        call dis::disassemble, { &disString }
        pha
        call str::print, { #7, local line, &disString }
        pla
        
    cmp #$20 ;last instruction JSR
    bne :-

    rts
    

endfunc

; --------------------------------------------------------------------------------------------

func disSelf

    locals
        line   .byte
    endlocals

    jPrintSetCursor 3, 8
    jPrint "For something to do to show function call logging and for fun:"
    jPrint
    jPrint "Disassembly of the routine that performs the disassembly."
    call ppu::transferVramBuffer
    jPrint "Turn on verbose mode (Press B) to see the calls made to generate the next screen."
    jPrint
    jPrint "This slows down emulation significantly and should only be used for debugging."

    call wait

    ; disassemble the disassembly routine 
    call dis::setAddress, { &(dis::disassemble+1)} ; + 1 skip debugging nop
    
    start:
    
    lda #3
    sta local line
    :
        inc local line
        
        call dis::disassemble, { &disString }
        pha
        
        call str::print, { #7, local line, &disString }
        
        ; every 4th loop, empty the buffer out to the screen
        lda local line
        and #$03
        bne :+
            call ppu::transferVramBuffer
        :
        pla
    
    beq exit ; exit on disassembly of BRK
    lda local line
    cmp #25
    bne :--
    
    call wait
    jmp start
    
    exit:
    rts
endfunc

; ----------------------------------------------------------------------------------------------

.endscope
    