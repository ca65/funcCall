
; main.s
; main file
; --------------------------------------------------------------------------------------------
; cl65 -t none -c src\main.s src\display.s
; cl65 -t none --mapfile main.map -Wl --dbgfile,main.nes.dbg -o main.nes -C nrom.cfg src\main.o src\display.o
; "..\..\..\nintendulatordebug\Nintendulator.exe" main.nes

.include "config.h"                     ; config.h .included in every file
.include "header/funcCall.h"            ; function calling macros

.segment "ZEROPAGE"
     .res 16        ; reserve space for function macros
    

; --------------------------------------------------------------------------------------------
; INES Header:
.include "header/inesheader.h"      
; --------------------------------------------------------------------------------------------
.segment "CODE"
; --------------------------------------------------------------------------------------------
; Libraries:

.include "library/ppu.inc"       
.include "library/nmi.inc"     
.include "library/reset.inc"
.include "library/controller.inc"
.include "library/string.inc"
.include "library/dis.inc"


.proc IRQ
    : jmp :-
.endproc


.include "header\nmi.h"     
.include "header\reset.h"

.segment "VECTORS"
    .word nmi::NMIRoutine, reset::resetRoutine, IRQ

.segment "CHRROM"
.incbin "CHR\c64.chr"