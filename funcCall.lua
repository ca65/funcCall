-----------------------------------------------------------------------------------------------
-- File: funcCall.lua
-----------------------------------------------------------------------------------------------
-- Track functions calls to ensure no parameter/locals are conflicting
-----------------------------------------------------------------------------------------------
-- To use:
-- You must include ndxdebug.h in your ca65 project:
-- .include "ndxdebug.h"
-- Load this file into NintendulatorDX manually, or with the macro statement:
-- ndxLuaExecFile "funcCall.lua"
-- The above line must be somewhere it will be executed with the CPU code. (Suggested: in your reset routine.)
-- These routines work with the funcCall.h macros to track the usage of the zeropage memory used for function
-- parameters and local variables to warn of possible conflicts, as well as track and warn of function
-- calls returning from the wrong function.

print ('Lua: Function tracking module loaded.')

-- options:

functionCallTracking = 1         -- set to 1 for this module to track and warn of conflict for function calls and parameter/locals usage
debugMessages = 0                -- set to 1 for a lot more info on calls

-- Get values from ca65 constants for what memory it to be used for parameters/locals.
-- don't change these

memoryStart = SYM._PARAMETER_MEMORY_START_[1]
memoryEnd   = SYM._PARAMETER_MEMORY_START_[1] + SYM._PARAMETER_MEMORY_MAXSIZE_[1] - 1

-----------------------------------------------------------------------------------------------
-- resetFunctionTracking: Reset this module 
-- This should be called on NES soft reset

function resetFunctionTracking ( )
    funcStack = {}
    memoryInUse = {}
    funcStackIndex = 0
    functionCallTracking = 1
    tabs = 0
    -- initialize memory in use
    for i = memoryStart, memoryEnd, 1
    do 
        memoryInUse [i] = 0
    end
end

-----------------------------------------------------------------------------------------------
-- Call reset when file is loaded (this will be executed when file is loaded in the emulator).

resetFunctionTracking ( )

-----------------------------------------------------------------------------------------------
-- Tab output for structured listing nested calls in debug mode

function tabPrint ( message )
    if debugMessages == 1 then
        -- correct for when tracking is turned on and off
        if tabs < 0 then tabs = 0 end
        print ( string.rep(" ", tabs) .. message )
    else
        print ( message )
    end
end

-----------------------------------------------------------------------------------------------
-- Display the last five functions called when there is a warning.

function printCalls ( )

    tabPrint ( 'Call Stack: (max 5)' )
    copyfuncStackIndex = funcStackIndex + 1  -- plus one to fix the initial decrement in the loop
    lastFive = 5
    repeat
        -- find a valid call:
        repeat
            copyfuncStackIndex = copyfuncStackIndex - 1
        until ( copyfuncStackIndex == 0 ) or (funcStack[ copyfuncStackIndex ].functionName ~= '') 
        if copyfuncStackIndex ~= 0 then 
            tabPrint ( string.format ('Function: %s, at address $%04X. Parameter Size: $%02X Locals Size: $%02X Released: %d', funcStack[ copyfuncStackIndex ].functionName, funcStack[ copyfuncStackIndex ].PCaddr, funcStack[ copyfuncStackIndex ].paramSize, funcStack[ copyfuncStackIndex ].localsSize, funcStack[ copyfuncStackIndex ].released))            
            lastFive = lastFive - 1
        end
    until copyfuncStackIndex == 0 or lastFive == 0
    tabPrint ( '------------------------')
end

-----------------------------------------------------------------------------------------------
-- Display a warning and other info for memory conflicts.

function memoryErrorDisplay ( memoryError )

    tabPrint( '------------------------' )
    tabPrint( memoryErrorString )
    tabPrint( 'Memory in use before call: ( X = byte reserved)' )
    
    memMapString = string.format ('$%02X->', memoryStart)
    for i = memoryStart, memoryEnd, 1
    do 
        if memoryInUse [i] ~= 0 then
            memMapString = memMapString .. '[X]'
        else
            memMapString = memMapString .. '[ ]'
        end
    end
    memMapString = memMapString .. string.format ('<-$%02X', memoryEnd) 
    tabPrint( memMapString )
    printCalls()
end

-----------------------------------------------------------------------------------------------
-- function functionEntry
-- Called when a function is executed. Parameter start and size is passed.
-- This function needs to verify the first address used for its parameters is not already used by the previous function.
-- library mode calls will also have the correct values for thisParamStart, thisParamSize

function functionEntry ( functionName, thisParamStart, thisParamSize, libMode )
    if functionCallTracking == 1 then
        
        if debugMessages == 1 then
            if thisParamSize > 0 then
                tabPrint (string.format('Entered %s at PC: $%04X. Reserved $%02X bytes for parameters starting at $%02X.', functionName, REG.PC, thisParamSize, thisParamStart ))
            else
                tabPrint (string.format('Entered %s at PC: $%04X. No parameter memory reserved.', functionName, REG.PC ))
            end
            tabs = tabs + 3
        end
        
        thisMemStart = thisParamStart        
        -- default value:
        thisMemEnd   = thisParamStart
        
        if thisParamSize > 0 then
            thisMemEnd   = thisParamStart + thisParamSize - 1
            -- check if any of the memory this function wants is marked as in use:
            for i = thisMemStart, thisMemEnd, 1
            do 
                if memoryInUse [i] ~= 0 then
                    memoryErrorString = string.format('Function %s parameter memory conflict. Memory requested: $%02X to $%02X. Called from %s at ($%04X). Memory location $%02X reserved by %s', 
                                            functionName, thisMemStart, thisMemEnd, funcStack[ funcStackIndex ].functionName, funcStack[ funcStackIndex ].PCaddr, i, funcStack[ memoryInUse[ i ] ].functionName )
                    memoryErrorDisplay (memoryErrorString)
                    offset = funcStack[ memoryInUse[ i ] ].paramSize + funcStack[ memoryInUse[ i ] ].localsSize
                    tabPrint ( string.format('Function %s has reserved $%02X bytes. Try changing the offset: func %s, { }, %d', funcStack[ memoryInUse[ i ] ].functionName, offset, functionName, offset))
                    tabPrint( '------------------------' )
                    break
                end
                -- mark memory with the index of the function that has claimed it:
                memoryInUse [i] = funcStackIndex + 1
            end
        end
        
        funcStackIndex = funcStackIndex + 1
        if funcStack[ funcStackIndex ]  == nil then
            funcStack [funcStackIndex] = { _functionName = '' ,  _memStart = 0 , _memEnd = 0 , _localsSize = 0, _released = 0, _PCaddr = 0, _tailCall = 0, _libMode = 0 }
        end

        funcStack[ funcStackIndex ].functionName = functionName
        funcStack[ funcStackIndex ].memStart = thisParamStart 
        funcStack[ funcStackIndex ].memEnd = thisMemEnd
        funcStack[ funcStackIndex ].paramSize = thisParamSize
        funcStack[ funcStackIndex ].localsSize = 0
        funcStack[ funcStackIndex ].released = 0
        funcStack[ funcStackIndex ].tailCall = 0
        funcStack[ funcStackIndex ].libMode = libMode
        funcStack[ funcStackIndex ].PCaddr = REG.PC
   end
end

-----------------------------------------------------------------------------------------------
-- function addLocalMemory
-- Add locals to a functions claimed memory. Also check that the memory requested is not already claimed.

function addLocalMemory (functionName, localsSize, newParamstart ) -- invoke after locals defined - newParamstart - if the locals macro adjusted thisParamStart
   if functionCallTracking == 1 then
   
        -- not sure how this could happen...but just in case
        --  if funcStack[ funcStackIndex ].functionName ~= functionName then
        --      tabPrint('Locals declaration mismatch with last function called.')
        --  end
        
        -- assume localsSize is greater than zero? we could check for this, but no one should be adding locals with 0 size
        
        if funcStack[ funcStackIndex ].libMode == 0 then
            -- find the memory being used for locals, also adjust memEnd
            thisMemStart = funcStack[ funcStackIndex ].memStart + funcStack[ funcStackIndex ].paramSize
            thisMemEnd   = thisMemStart + localsSize - 1
            funcStack[ funcStackIndex ].memEnd = thisMemEnd
        else
            -- library mode
            thisMemStart = newParamstart
            thisMemEnd   = thisMemStart + localsSize - 1
            funcStack[ funcStackIndex ].memStart = newParamstart
        end
        
        -- check if any of the local memory this function wants is marked as in use:
        for i = thisMemStart, thisMemEnd, 1
        do 
            if memoryInUse [i] ~= 0 then
                memoryErrorString = string.format('Function %s locals memory conflict. Local memory requested for this function: $%02X to $%02X. Called from %s at ($%04X). Memory location $%02X reserved by %s', 
                                        functionName, thisMemStart, thisMemEnd, funcStack[ funcStackIndex ].functionName, funcStack[ funcStackIndex ].PCaddr, i, funcStack[ memoryInUse[ i ] ].functionName )
                memoryErrorDisplay (memoryErrorString)
                offset = funcStack[ memoryInUse[ i ] ].paramSize + funcStack[ memoryInUse[ i ] ].localsSize
                tabPrint ( string.format('Function %s has reserved $%02X bytes. Try changing the offset: func %s, { }, %d', funcStack[ memoryInUse[ i ] ].functionName, offset, functionName, offset))
                tabPrint ( '------------------------' )
                break
            end
            -- mark memory with the index of the function that has claimed it:
            memoryInUse [i] = funcStackIndex
        end
        
        if debugMessages == 1 then
            tabPrint (string.format('%s: Reserved $%02X bytes for local variables starting at $%02X.', functionName, localsSize, thisMemStart )) 
        end
        funcStack[funcStackIndex].localsSize = localsSize
   end
end

-----------------------------------------------------------------------------------------------
-- function releaseMemory
-- Release all memory for the currently active function. 
-- For exiting a function or for calling another function if the memory is not needed at the time.

function releaseMemory ( functionName )  -- release memory
    if functionCallTracking == 1 then
        if funcStack[ funcStackIndex ].released == 0 then
            for i = funcStack[ funcStackIndex ].memStart, funcStack[ funcStackIndex ].memEnd, 1
            do 
                -- make sure a previous function doesn't have claim to this memory:
                if memoryInUse [i] == funcStackIndex then
                    memoryInUse [i] = 0
                end
            end
            funcStack[funcStackIndex].released = 1
        end    
    end    
end

-----------------------------------------------------------------------------------------------
-- function reclaimMemory
-- Reclaim all memory for the currently active function. 

function reclaimMemory ( functionName ) -- reverse release of memory 
    if functionCallTracking == 1 then
        if funcStack[ funcStackIndex ].released == 1 then
            for i = funcStack[ funcStackIndex ].memStart, funcStack[ funcStackIndex ].memEnd, 1
            do 
                if memoryInUse [i] == 0 then
                    memoryInUse [i] = funcStackIndex
                end
            end
            funcStack[funcStackIndex].released = 0
        end
    end    
end

-----------------------------------------------------------------------------------------------
-- function returnFromFunc
-- Return from a function: release all memory, move the stack counter up.
-- Check if the function returned from was expected.

function returnFromFunc ( functionName ) -- call this on return from jsr call in the 'call' macro
    if functionCallTracking == 1 then
    
        -- check for tail calls:
        -- if previous function marked a tail call, it means this returning function was called by a tail call
        
        while ( funcStackIndex - 1 ) > 0 and funcStack[ funcStackIndex - 1 ].tailCall == 1 do
            if debugMessages == 1 then
                tabs = tabs - 3
                tabPrint (string.format('Returned from tail call to: %s at PC: $%04X', funcStack[ funcStackIndex ].functionName, REG.PC ))
            end
            releaseMemory ( funcStack[ funcStackIndex ].functionName )
            funcStackIndex = funcStackIndex - 1
        end
        
        if debugMessages == 1 then    
            tabs = tabs - 3
            tabPrint (string.format('Returned from call to: %s at PC: $%04X', functionName, REG.PC ))
            tabPrint ('------------------------')
        end
        
        if funcStack[ funcStackIndex ].functionName ~= functionName then
            if debugMessages == 1 then    
                tabs = tabs - 3
            end
            tabPrint( string.format('Return mismatch with last function called: Returned from %s, Last function entered: %s.', functionName, funcStack[ funcStackIndex ].functionName ))
            printCalls()
            while funcStack[ funcStackIndex ].functionName ~= functionName do
                releaseMemory (functionName)
                funcStack[ funcStackIndex ].functionName = ''  -- mark as unused for printCalls
                funcStackIndex = funcStackIndex - 1
            end
        end
        releaseMemory (functionName)
        funcStack[ funcStackIndex ].functionName = ''  -- mark as unused for printCalls
        funcStackIndex = funcStackIndex - 1
    end    
end

-----------------------------------------------------------------------------------------------
-- Exit a function with a tail call.
-- This means that this function 'called' another function, rather than returned.
-- Its memory is released, but it is left on the stack, since it hasn't returned yet, but called another function.

function tailCall ( functionName )
    if functionCallTracking == 1 then
        releaseMemory (functionName)
        if funcStack [funcStackIndex ].tailCall == 0 then
            if debugMessages == 1 then
                tabPrint (string.format('Tail call: Exiting %s at PC: $%04X', functionName, REG.PC))
            end
            funcStack [funcStackIndex ].tailCall = 1
        end    
    end    
end

-----------------------------------------------------------------------------------------------

function functionCallTrackingToggle ( on )
    functionCallTracking = on
end
-----------------------------------------------------------------------------------------------

function functionCallTrackingVerbose ( on )
    debugMessages = on
end
